<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_revenues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian');
            $table->bigInteger('revenues');
            $table->string('name', 100);
            $table->string('jenis_royalty', 100);
            $table->double('jumlah_royalty', 10, 2);
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
            $table->foreign('revenues')->references('id')->on('unit_bisnis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_revenues');
    }
}
