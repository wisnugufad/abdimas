<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianMitraOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_mitra_outputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian');
            $table->bigInteger('mitra_output');
            $table->string('name', 100);
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
            $table->foreign('mitra_output')->references('id')->on('mitra_outputs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_mitra_outputs');
    }
}
