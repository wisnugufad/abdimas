<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianPendanaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_pendanaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian')->nullable();
            $table->bigInteger('dana')->nullable();
            $table->string('name', 100)->nullable();
            $table->decimal('jumlah',12,0)->nullable()->default(0);
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
            $table->foreign('dana')->references('id')->on('dana_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_pendanaans');
    }
}
