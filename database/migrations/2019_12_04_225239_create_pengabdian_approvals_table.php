<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian');
            $table->string('status', 10);
            $table->bigInteger('user');
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
            $table->foreign('user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_approvals');
    }
}
