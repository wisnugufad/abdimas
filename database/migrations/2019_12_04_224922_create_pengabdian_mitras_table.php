<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianMitrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_mitras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian')->nullable();
            $table->bigInteger('mitra')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('alamat', 200)->nullable();
            $table->string('product', 200)->nullable();
            $table->string('asset', 200)->nullable();
            $table->double('omset' , 10, 2)->nullable();
            $table->integer('jumlah_karyawan')->nullable();
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
            $table->foreign('mitra')->references('id')->on('mitra_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_mitras');
    }
}
