<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdianProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdian_proposals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengabdian');
            $table->string('name', 200);
            $table->string('hash_name', 200);
            $table->string('directory', 100);
            $table->string('path', 100);
            $table->timestamps();

            $table->foreign('pengabdian')->references('id')->on('pengabdians')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdian_proposals');
    }
}
