<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengabdiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengabdians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pengabdian', 100);
            $table->char('tahun',4);
            $table->string('judul');
            $table->string('bidang', 200);
            $table->bigInteger('dosen');
            $table->bigInteger('kode_prodi');
            $table->bigInteger('semester');
            $table->decimal('total',5,2);
            $table->timestamps();

            $table->foreign('dosen')->references('id')->on('dosens');
            $table->foreign('kode_prodi')->references('id')->on('prodis');
            $table->foreign('semester')->references('id')->on('semesters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengabdians');
    }
}
