<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user');
            $table->string('nidn', 10)->unique();
            $table->string('name');
            $table->string('gelar', 15);
            $table->date('tgl_lahir');
            $table->string('email')->nullable()->unique();
            $table->string('telp', 12)->nullable()->unique();
            $table->text('alamat')->nullable();
            $table->bigInteger('kepangkatan');
            $table->bigInteger('prodi');
            // $table->bigInteger('pendidikan');
            // $table->bigInteger('role');
            $table->timestamps();

            // $table->foreign('pendidikan')->references('id')->on('pendidikan_masters');
            // $table->foreign('role')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('kepangkatan')->references('id')->on('kepangkatans');
            $table->foreign('prodi')->references('id')->on('prodis');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosens');
    }
}
