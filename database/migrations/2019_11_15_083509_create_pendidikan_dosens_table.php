<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendidikanDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikan_dosens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('dosen');
            $table->bigInteger('pendidikan');
            $table->string('univ', 100);
            $table->string('jurusan', 100);
            $table->timestamps();

            $table->foreign('pendidikan')->references('id')->on('pendidikan_masters');
            $table->foreign('dosen')->references('id')->on('dosens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendidikan_dosens');
    }
}
