<?php

use Illuminate\Database\Seeder;
use App\Models\PendidikanMaster;

class PendidikanMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pendidikan = array(
            array(
                'kode'=>'D1',
                'pendidikan'=>'Diploma Satu',
                'description'=>'Gelar Ahli Pratama (AP)',
            ),
            array(
                'kode'=>'D2',
                'pendidikan'=>'Diploma Dua',
                'description'=>'Gelar Ahli Muda (A.Ma)',
            ),
            array(
                'kode'=>'D3',
                'pendidikan'=>'Diploma Satu',
                'description'=>'Gelar Ahli Madya (A.Md)',
            ),
            array(
                'kode'=>'D4',
                'pendidikan'=>'Diploma Empat',
                'description'=>'Gelar Sarjana Ahli Terapan (S.St)',
            ),
            array(
                'kode'=>'S1',
                'pendidikan'=>'Starta Satu',
                'description'=>'Gelar sesuai Jurusan yang dipilih',
            ),
            array(
                'kode'=>'S2',
                'pendidikan'=>'Starta Dua',
                'description'=>'Gelar sesuai Jurusan yang dipilih',
            ),
            array(
                'kode'=>'S3',
                'pendidikan'=>'Starta Tiga',
                'description'=>'Gelar Doctor (Dr)',
            ),

        );

        foreach ($pendidikan as $item)
        {
            PendidikanMaster::create([
                'tingkat' => $item['kode'],
                'pendidikan' => $item['pendidikan'],
                'description' => $item['description'],
            ]);
        }
    }
}
