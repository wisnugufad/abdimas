<?php

use Illuminate\Database\Seeder;
use App\Models\Dosen;
use App\User;
use App\Models\PendidikanDosen;

class DosenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'name'=>'Endang Ratnawati Djuwitaningrum',
                'nidn'=>'316126003',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1960-12-16',
                'kepangkatan'=>4,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'UI Depok',
                        'jurusan'=>'Matematika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'UI Depok',
                        'jurusan'=>'Ilmu Komputer'
                    ),
                )
            ),

            array(
                'name'=>'Sunarto',
                'nidn'=>'307066502',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1965-7-6',
                'kepangkatan'=>3,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'STIMIK Budi Luhur',
                        'jurusan'=>'Manajemen Informatika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'STIMIK Budi Luhur',
                        'jurusan'=>'Informatika'
                    ),
                )
            ),

            array(
                'name'=>'Katri Widayani',
                'nidn'=>'13095901',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1959-09-13',
                'kepangkatan'=>4,
                'gelar'=>'MT',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'UPN Yogjakarta',
                        'jurusan'=>'Pertanian'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'ITB Bandung',
                        'jurusan'=>'Teknik Industri'
                    ),
                )
            ),

            array(
                'name'=>'Sumiarti Andri',
                'nidn'=>'310096101',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1961-09-10',
                'kepangkatan'=>5,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'IPB Bogor',
                        'jurusan'=>'Statistik'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'ST Benarif',
                        'jurusan'=>'Informatika'
                    ),
                )
            ),

            array(
                'name'=>'Sulistyowati',
                'nidn'=>'324056703',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1967-05-24',
                'kepangkatan'=>4,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'ITB Bandung',
                        'jurusan'=>'Matematika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'UI Depok',
                        'jurusan'=>'Ilmu Komputer'
                    ),
                )
            ),

            array(
                'name'=>'Melani Indriasari',
                'nidn'=>'309088101',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1981-08-09',
                'kepangkatan'=>3,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'ITI Serpong ',
                        'jurusan'=>'Informatika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'SGU Serpong',
                        'jurusan'=>'Informatika'
                    ),
                )
            ),

            array(
                'name'=>'Yustina Sri Suharini',
                'nidn'=>'329037101',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1971-03-29',
                'kepangkatan'=>5,
                'gelar'=>'MT',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'UNDIP Semarang',
                        'jurusan'=>'Teknik Elektro'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'ITB Bandung',
                        'jurusan'=>'Informatika'
                    ),
                )
            ),

            array(
                'name'=>'Husni',
                'nidn'=>'314077204',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1972-07-14',
                'kepangkatan'=>1,
                'gelar'=>'M Kom',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'ITI Serpong',
                        'jurusan'=>'Informatika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'SGU Serpong',
                        'jurusan'=>'Informatika'
                    ),
                )
            ),

            array(
                'name'=>'Suryo Bramasto',
                'nidn'=>'305128109',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1981-12-5',
                'kepangkatan'=>4,
                'gelar'=>'MT',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'UGM Yogyakarta',
                        'jurusan'=>'Teknik Elektro'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'ITB Bandung',
                        'jurusan'=>'Teknik Elektro'
                    ),
                )
            ),

            array(
                'name'=>'Muhammad Ramli',
                'nidn'=>'310068504',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1985-06-10',
                'kepangkatan'=>6,
                'gelar'=>'ST',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'ITI Serpong',
                        'jurusan'=>'Informatika'
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'UI Depok',
                        'jurusan'=>'Ilmu Komputer'
                    ),
                )
            ),

            array(
                'name'=>'Indrati Sukmadi',
                'nidn'=>'301095902',
                'password'=>bcrypt('iti123'),
                'tgl_lahir'=>'1959-09-01',
                'kepangkatan'=>4,
                'gelar'=>'M Sc',
                'prodi'=>5,
                'role'=>2,
                'pendidikan'=>array(
                    array(
                        'pendidikan'=>5,
                        'univ'=>'UI Depok',
                        'jurusan'=>'Matematika '
                    ),
                    array(
                        'pendidikan'=>6,
                        'univ'=>'Univ Southern Colorado at Pueblo USA',
                        'jurusan'=>'Sistem Engineering'
                    ),
                )
            ),
        );

        foreach ($data as $item)
        {
            $user = User::create([
                'name' => $item['name'],
                'email'=>$item['nidn'],
                'email_verified_at' => now(),
                'password' => $item['password'],
                'role'=>$item['role']
            ]);

            $dosen = Dosen::create([
                'user' => $user->id,
                'nidn' => $item['nidn'],
                'name' => $item['name'],
                'gelar' => $item['gelar'],
                'prodi' => $item['prodi'],
                'tgl_lahir'=> date('Y-m-d',strtotime($item['tgl_lahir'])),
                'kepangkatan'=> $item['kepangkatan'],
            ]);

            foreach ($item['pendidikan'] as $value) {
                PendidikanDosen::create([
                    'dosen'=>$dosen->id,
                    'pendidikan'=>$value['pendidikan'],
                    'univ'=>$value['univ'],
                    'jurusan'=>$value['jurusan'],
                ]);
            }
        }
    }
}
