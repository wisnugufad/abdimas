<?php

use Illuminate\Database\Seeder;
use App\Models\Typical;

class TypicalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typical = array(
            array(
                'typical'=>'Teknologi Tepat Guna',
                'bobot'=>5,
            ),
            array(
                'typical'=>'Model',
                'bobot'=>5,
            ),
            array(
                'typical'=>'Prototipe/Purwarupa',
                'bobot'=>5,
            ),
            array(
                'typical'=>'Karya Desain',
                'bobot'=>5,
            ),
            array(
                'typical'=>'Seni Karya, Bangunan dan Arsitek',
                'bobot'=>5,
            ),
        );

        foreach ($typical as $item)
        {
            Typical::create([
                'typical' => $item['typical'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
