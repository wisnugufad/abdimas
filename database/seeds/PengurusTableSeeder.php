<?php

use Illuminate\Database\Seeder;
use App\Models\Pengurus;

class PengurusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengurus = array(
            'name' => 'Dr.Ir. Iyus Hendrawan, MSi, IPU',
            'nidn' => '0330066102',
            'email' => 'iyus.hendrawan@iti.ac.id',
            'telp' => '',
            'alamat' => ''
        );

        Pengurus::create([
            'name' => $pengurus['name'],
            'nidn' => $pengurus['nidn'],
            'email' => $pengurus['email'],
            'telp' => $pengurus['telp'],
            'alamat' => $pengurus['alamat']
        ]);
    }
}
