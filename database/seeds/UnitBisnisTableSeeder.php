<?php

use Illuminate\Database\Seeder;
use App\Models\UnitBisnis;

class UnitBisnisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typical = array(
            array(
                'typical'=>'Unit Bisnis Berbasis Product',
                'bobot'=>7.5,
            ),
            array(
                'typical'=>'Unit Bisnis Berbasis Jasa',
                'bobot'=>7.5,
            ),
        );

        foreach ($typical as $item)
        {
            UnitBisnis::create([
                'unit_bisnis' => $item['typical'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
