<?php

use Illuminate\Database\Seeder;
use App\Models\IptekMaster;

class IptekMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iptek = array(
            array(
                'iptek'=>'Paten',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Paten Sederhana',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Perlindungan Varietas Tanaman',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Hak Cipta',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Rahasia Dagang',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Merk Dagang',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Desain Product Industri',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Indikasi Geografis',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Perlindungan Desain Tata Letak Sirkuit Terpadu',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'TTG',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Model',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Prototype/Purwarupa',
                'bobot'=>2.5,
            ),
            array(
                'iptek'=>'Karya Desain, Seni Karya, Bangunan/Arsitektur',
                'bobot'=>2.5,
            ),
        );

        foreach ($iptek as $item)
        {
            IptekMaster::create([
                'iptek' => $item['iptek'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
