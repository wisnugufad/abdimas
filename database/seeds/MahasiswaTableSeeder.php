<?php

use Illuminate\Database\Seeder;
use App\Models\Mahasiswa;

class MahasiswaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mahasiswa = array(
            array(
                'nrp'=>'1151400074',
                'name'=>'Wisnu Gufa Destiawan',
                'email'=>'wisnugufad@gmail.com',
                'alamat'=>'Perum Griya Bunga Asri Block O no 6',
                'telp'=>'089643581656',
                'prodi'=>5,
            )
        );

        foreach ($mahasiswa as $item)
        {
            Mahasiswa::create([
                'nrp' => $item['nrp'],
                'name' => $item['name'],
                'email' => $item['email'],
                'alamat' => $item['alamat'],
                'prodi' => $item['prodi']
            ]);
        }
    }
}
