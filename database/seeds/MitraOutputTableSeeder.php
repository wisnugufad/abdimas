<?php

use Illuminate\Database\Seeder;
use App\Models\MitraOutput;

class MitraOutputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mitra = array(
            array(
                'mitra'=>'Paten',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Paten Sederhana',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Perlindungan Varietas Tanaman',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Hak Cipta',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Rahasia Dagang',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Merk Dagang',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Desain Product Industri',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Indikasi Geografis',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Perlindungan Desain Tata Letak Sirkuit Terpadu',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'TTG',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Model',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Prototype/Purwarupa',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Karya Desain, Seni Karya, Bangunan/Arsitektur',
                'bobot'=>2.5,
            ),
            array(
                'mitra'=>'Produk Tersertifikasi',
                'bobot'=>1.5,
            ),
            array(
                'mitra'=>'Produk Terstandarisasi',
                'bobot'=>1.5,
            ),
            array(
                'mitra'=>'Mitra Berbadan Hukum',
                'bobot'=>1.5,
            ),
        );

        foreach ($mitra as $item)
        {
            MitraOutput::create([
                'mitra' => $item['mitra'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
