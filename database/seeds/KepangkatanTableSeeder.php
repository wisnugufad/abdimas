<?php

use Illuminate\Database\Seeder;
use App\Models\Kepangkatan;

class KepangkatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pangkat = array(
            'Pengajar',
            'Guru Besar',
            'Asisten Ahli',
            'Lektor',
            'Lektor Kepala',
            'Tugas Belajar'
        );

        foreach ($pangkat as $item)
        {
            Kepangkatan::create([
                'pangkat' => $item,
            ]);
        }
    }
}
