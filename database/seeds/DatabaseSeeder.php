<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(KepangkatanTableSeeder::class);
        $this->call(PendidikanMasterTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProdiTableSeeder::class);
        $this->call(DosenTableSeeder::class);
        $this->call(MahasiswaTableSeeder::class);

        $this->call(PengurusTableSeeder::class);
        $this->call(SemesterTableSeeder::class);
        $this->call(MitraMasterTableSeeder::class);
        $this->call(MitraOutputTableSeeder::class);
        $this->call(PublikasiMasterTableSeeder::class);
        $this->call(DanaMasterTableSeeder::class);
        $this->call(IptekMasterTableSeeder::class);
        $this->call(TypicalTableSeeder::class);
        $this->call(UnitBisnisTableSeeder::class);
    }
}
