<?php

use Illuminate\Database\Seeder;
use App\Models\MitraMaster;

class MitraMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mitra = array(
            array(
                'mitra'=>'Mitra yang Non Produktif',
                'bobot'=>5,
            ),
            array(
                'mitra'=>'Mitra yang Produktif',
                'bobot'=>6
            ),
            array(
                'mitra'=>'Mitra CSR/Pemda/Industri (UKM)',
                'bobot'=>7
            ),
            array(
                'mitra'=>'Mitra Produksinya Meningkat',
                'bobot'=>9
            ),
            array(
                'mitra'=>'Mitra yang Kualitas Produknya Meningkat',
                'bobot'=>10.0
            ),
            array(
                'mitra'=>'Mitra yang Berhasil Melakukan Ekspor atau Antar Pulau',
                'bobot'=>9
            ),
            array(
                'mitra'=>'Mitra yang Menghasilkan Usahawan Muda',
                'bobot'=>6
            ),
            array(
                'mitra'=>'Mitra yang Omsetnya Meningkat',
                'bobot'=>8
            ),
            array(
                'mitra'=>'Mitra yang Tenaga Kerjanya Meningkat',
                'bobot'=>9
            ),
            array(
                'mitra'=>'Mitra yang Kemampuan Manajemennya Meningkat',
                'bobot'=>6
            ),
        );

        foreach ($mitra as $item)
        {
            MitraMaster::create([
                'mitra' => $item['mitra'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
