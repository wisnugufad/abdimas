<?php

use Illuminate\Database\Seeder;

class PengabdianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $abdimas = array(
            array(
                'judul'=>'Mengajar Program Pelatihan Learning AWS 2019 Digital Talent Scholarship',
                'bidang'=>'Pelatihan Komputer',
                'dosen'=>9,
                'dana'=>7,
                'anggota'=>array(
                    array(
                        'anggota'=>10,
                        'role'=>3
                    ),
                    array(
                        'anggota'=>1,
                        'role'=>3
                    ),
                )
            ),
            array(
                'judul'=>'Pembinaan Teknologi informasi komunikasi untuk usaha kecil menengah dengan strategis menguasai pasar dengan basis digital marketing',
                'bidang'=>'Multimedia',
                'dosen'=>9,
                'dana'=>7,
                'anggota'=>array(
                    array(
                        'anggota'=>10,
                        'role'=>3
                    ),
                    array(
                        'anggota'=>1,
                        'role'=>3
                    ),
                )
            ),
        );
    }
}
