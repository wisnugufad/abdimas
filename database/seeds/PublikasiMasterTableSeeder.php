<?php

use Illuminate\Database\Seeder;
use App\Models\PublikasiMaster;

class PublikasiMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $publikasi = array(
            array(
                'publikasi'=>'Artikel di Jurnal Internasional',
                'bobot'=>5
            ),
            array(
                'publikasi'=>'Artikel di Jurnal Nasional',
                'bobot'=>3
            ),
            array(
                'publikasi'=>'Artikel di Jurnal Lokal',
                'bobot'=>2
            ),
            array(
                'publikasi'=>'Tulisan/Berita di Media International',
                'bobot'=>2
            ),
            array(
                'publikasi'=>'Tulisan/Berita di Media Nasional',
                'bobot'=>1.5
            ),
            array(
                'publikasi'=>'Makalah di Forum Ilmiah Internasional',
                'bobot'=>3
            ),
            array(
                'publikasi'=>'Makalah di Forum Ilmiah Nasional',
                'bobot'=>1.5
            ),
            array(
                'publikasi'=>'Makalah di Forum Ilmiah Regional',
                'bobot'=>1
            ),
        );

        foreach ($publikasi as $item)
        {
            PublikasiMaster::create([
                'publikasi' => $item['publikasi'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
