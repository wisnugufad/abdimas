<?php

use Illuminate\Database\Seeder;
use App\Models\Prodi;

class ProdiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prodi = array(
            array(
                'kode_jurusan'=>'111',
                'nama_jurusan'=>'Teknik Electro',
                'kode_nama'=>'EL',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'112',
                'nama_jurusan'=>'Teknik Mesin & Otomotif',
                'kode_nama'=>'M',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'113',
                'nama_jurusan'=>'Teknik Industri',
                'kode_nama'=>'TI',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'114',
                'nama_jurusan'=>'Teknik Kimia',
                'kode_nama'=>'TK',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'115',
                'nama_jurusan'=>'Informatika',
                'kode_nama'=>'IF',
                'kaprodi'=>5
            ),
            array(
                'kode_jurusan'=>'121',
                'nama_jurusan'=>'Teknik Sipil',
                'kode_nama'=>'SP',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'122',
                'nama_jurusan'=>'Teknik Arsitektur',
                'kode_nama'=>'ARS',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'123',
                'nama_jurusan'=>'Teknik Perancangan Wilayah Kota',
                'kode_nama'=>'PWK',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'131',
                'nama_jurusan'=>'Teknik Industri Pertanian',
                'kode_nama'=>'TIP',
                'kaprodi'=>null
            ),
            array(
                'kode_jurusan'=>'141',
                'nama_jurusan'=>'Teknik Management',
                'kode_nama'=>'MGN',
                'kaprodi'=>null
            ),
        );

        foreach ($prodi as $item)
        {
            Prodi::create([
                'kode_prodi' => $item['kode_jurusan'],
                'nama_prodi' => $item['nama_jurusan'],
                'kode_nama'=> $item['kode_nama'],
                'nidn' => $item['kaprodi']
            ]);
        }
    }
}
