<?php

use Illuminate\Database\Seeder;
use App\Models\Semester;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = array(
            'SEMESTER GENAP 2018/2019',
            'SEMESTER GANJIL 2019/2020',
            'SEMESTER GENAP 2019/2020',
        );

        foreach ($role as $item)
        {
            Semester::create([
                'semester' => $item,
            ]);
        }
    }
}
