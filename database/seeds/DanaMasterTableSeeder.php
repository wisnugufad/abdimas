<?php

use Illuminate\Database\Seeder;
use App\Models\DanaMaster;

class DanaMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dana = array(
            array(
                'dana'=>'DRPM Mono Year',
                'bobot'=>2,
            ),
            array(
                'dana'=>'DRPM Multi Year',
                'bobot'=>4,
            ),
            array(
                'dana'=>'Non DRPM Internal PT',
                'bobot'=>4,
            ),
            array(
                'dana'=>'Non DRPM Pemda',
                'bobot'=>2.5,
            ),
            array(
                'dana'=>'Non DRPM CRS',
                'bobot'=>2.5,
            ),
            array(
                'dana'=>'Dana Luar Negeri',
                'bobot'=>2.5,
            ),
            array(
                'dana'=>'Depkominfo',
                'bobot'=>2.5,
            ),
            array(
                'dana'=>'Mandiri',
                'bobot'=>2.5,
            ),
            array(
                'dana'=>'LP2M ITI',
                'bobot'=>2.5,
            ),
        );

        foreach ($dana as $item)
        {
            DanaMaster::create([
                'dana' => $item['dana'],
                'bobot' => $item['bobot'],
            ]);
        }
    }
}
