<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $guarded = [];

    public function pendidikan()
    {
        return $this->hasMany(PendidikanDosen::class,'dosen');
    }
}
