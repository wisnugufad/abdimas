<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengabdian extends Model
{
    protected $guarded = [];

    public function anggota()
    {
        return $this->hasMany(PengabdianAnggota::class,'pengabdian');
    }

    public function dana()
    {
        return $this->hasMany(PengabdianPendanaan::class,'pengabdian');
    }

    public function iptek()
    {
        return $this->hasMany(PengabdianIptek::class,'pengabdian');
    }

    public function publikasi()
    {
        return $this->hasMany(PengabdianPublikasi::class,'pengabdian');
    }

    public function buku()
    {
        return $this->hasMany(PengabdianBuku::class,'pengabdian');
    }

    public function mitra()
    {
        return $this->hasMany(PengabdianMitra::class,'pengabdian');
    }

    public function mitra_output()
    {
        return $this->hasMany(PengabdianMitraOutput::class,'pengabdian');
    }

    public function typical()
    {
        return $this->hasMany(PengabdianTypical::class,'pengabdian');
    }
}
