<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendidikanDosen extends Model
{
    protected $guarded = [];

    public function pendidikan()
    {
        return $this->belongsToMany(Dosen::class);
    }
}
