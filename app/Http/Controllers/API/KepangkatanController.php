<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kepangkatan;
use App\Http\Resources\KepangkatanCollection;

class KepangkatanController extends Controller
{
    public function index()
    {
        try {
            $keyword = request()->search;
            $data = Kepangkatan::orderBy('id','ASC');

            if (is_null($keyword)) {
                $kepangkatan = $data;
            } else {
                $kepangkatan = $data->where('kepangkatans.pangkat','ILIKE','%'.$keyword.'%');
            }
            $limit = request()->limit;
            return new KepangkatanCollection($kepangkatan->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $kepangkatan = Kepangkatan::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$kepangkatan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $kepangkatan = Kepangkatan::create([
                'pangkat' => $request->pangkat
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $kepangkatan = Kepangkatan::find($id);
            return response()->json(['status' => 'success','data'=>$kepangkatan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $kepangkatan = Kepangkatan::find($id);
            $kepangkatan->update([
                'pangkat' => $request->pangkat
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $kepangkatan = Kepangkatan::find($id);
            $kepangkatan->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
