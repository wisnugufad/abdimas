<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Prodi;
use App\Http\Resources\ProdiCollection;

class ProdiController extends Controller
{
    public function index()
    {
        try {
            $keyword = request()->search;
            $data = Prodi::orderBy('id','ASC')
            ->leftJoin('dosens','prodis.nidn','dosens.id')
            ->select('prodis.*','dosens.name as ka_prodi');

            if (is_null($keyword)) {
                $prodi = $data;
            } else {
                $prodi = $data->where('prodis.kode_prodi','ILIKE','%'.$keyword.'%')
                ->orWhere('prodis.kode_nama','ILIKE','%'.$keyword.'%')
                ->orWhere('prodis.nama_prodi','ILIKE','%'.$keyword.'%');
            }

            $limit = request()->limit;

            return new ProdiCollection($prodi->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $prodi = Prodi::orderBy('id','ASC')
            ->leftJoin('dosens AS b','prodis.nidn','b.id')
            ->select('prodis.id','prodis.kode_prodi','prodis.nama_prodi','prodis.kode_nama','b.name','b.gelar','b.nidn')
            ->get();
            return response()->json(['status' => 'success','data'=>$prodi]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $prodi = Prodi::create([
                'kode_prodi' => $request->kode_prodi,
                'nama_prodi' => $request->nama_prodi,
                'kode_nama'=>$request->kode_nama,
                'nidn' => $request->nidn,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $prodi = Prodi::find($id);
            return response()->json(['status' => 'success','data'=>$prodi]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $prodi = Prodi::find($id);
            $prodi->update([
                'kode_prodi' => $request->kode_prodi,
                'nama_prodi' => $request->nama_prodi,
                'kode_nama'=>$request->kode_nama,
                'nidn' => $request->nidn,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $prodi = Prodi::find($id);
            $prodi->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
