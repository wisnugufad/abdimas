<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Http\Resources\MahasiswaCollection;

class MahasiswaController extends Controller
{
    public function index()
    {
        try {
            $data = Mahasiswa::orderBy('id','ASC')
            ->leftJoin('prodis','prodis.id','mahasiswas.prodi')
            ->select('mahasiswas.*','prodis.nama_prodi');
            $limit=request()->limit;

            $keyword = request()->keyword;
            if ($keyword !== '' && $keyword !== "undefined") {
                $mahasiswa = $data->where('nrp','ILIKE','%'.$keyword.'%')
                ->orWhere('name','ILIKE','%'.$keyword.'%');
            }else{
                $mahasiswa = $data;
            }

            return new MahasiswaCollection($mahasiswa->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $data = Mahasiswa::orderBy('id','ASC');

            $keyword = request()->keyword;
            if ($keyword !== '' && $keyword !== "undefined") {
                $mahasiswa = $data->where('nrp','ILIKE','%'.$keyword.'%')
                ->orWhere('name','ILIKE','%'.$keyword.'%')->get();
            }else{
                $mahasiswa = $data->get();
            }
            return response()->json(['status' => 'success','data'=>$mahasiswa]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $mahasiswa = Mahasiswa::create([
                'nrp' => $request->nrp,
                'name' => $request->name,
                'email' => $request->email,
                'telp' => $request->telp,
                'alamat' => $request->alamat,
                'prodi' => $request->prodi
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $mahasiswa = Mahasiswa::find($id);
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $mahasiswa = Mahasiswa::find($id);
            $mahasiswa->update([
                'nrp' => $request->nrp,
                'name' => $request->name,
                'email' => $request->email,
                'telp' => $request->telp,
                'alamat' => $request->alamat,
                'prodi' => $request->prodi
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $mahasiswa = Mahasiswa::find($id);
            $mahasiswa->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
