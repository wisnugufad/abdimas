<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pengabdian;
use App\Models\PengabdianApproval;
use App\Models\PengabdianAnggota;
use App\Models\PengabdianPendanaan;
use App\Models\PengabdianIptek;
use App\Models\PengabdianPublikasi;
use App\Models\PengabdianBuku;
use App\Models\PengabdianMitra;
use App\Models\PengabdianMitraOutput;
use App\Models\PengabdianTypical;
use App\Models\PengabdianRevenue;
use App\Models\PengabdianProposal;
use DB;

class PengajuanController extends Controller
{
    public function Index()
    {
        # code...
    }

    public function create(Request $request)
    {

        DB::beginTransaction();
        try {

            $uploadedFile = $request->file('file');
            $extention = $uploadedFile->extension();

            if ($extention !== "pdf") {
                DB::rollback();
                return response()->json(['status' => 'failed','error' => 'file not supported'],500);
            }

            $pengajuan = Pengabdian::create([
                'kode_pengabdian'=> $request->kode_pengabdian,
                'bidang'=> $request->bidang,
                'tahun'=>$request->tahun,
                'semester'=>$request->semester,
                'judul'=> $request->judul,
                'kode_prodi'=> $request->kode_prodi,
                'dosen'=> $request->dosen,
                'total'=>$request->total
            ]);

            // Apprval
            PengabdianApproval::create([
                'pengabdian'=>$pengajuan->id,
                'status'=> $request->approval,
                'user'=> $request->user,
            ]);

            // // Anggota

            if ($request->has('anggota')) {
                foreach ($request->anggota as $value) {
                    if ($value['status'] === 'dosen') {
                        $role = 2;
                    }else{
                        $role = 3;
                    }
                    PengabdianAnggota::create([
                        'pengabdian'=>$pengajuan->id,
                        'anggota'=>$value['id'],
                        'role'=> $role
                    ]);
                }
            }


            // Pendanaan
            if ($request->has('dana')) {
                foreach ($request->dana as $value) {
                    PengabdianPendanaan::create([
                        'pengabdian'=>$pengajuan->id,
                        'dana'=>$value['id'],
                        'name'=>$value['name'],
                        'jumlah'=>$value['jumlah']
                    ]);
                }
            }


            // Iptek
            if ($request->has('iptek')) {
                foreach ($request->iptek as $value) {
                    PengabdianIptek::create([
                        'pengabdian'=>$pengajuan->id,
                        'iptek'=>$value['id'],
                        'name'=>$value['name']
                    ]);
                }
            }

            // Publikasi
            if ($request->has('publikasi')) {
                foreach ($request->publikasi as $value) {
                    PengabdianPublikasi::create([
                        'pengabdian'=>$pengajuan->id,
                        'publikasi'=>$value['id'],
                        'name'=>$value['name']
                    ]);
                }
            }

            // Buku
            if ($request->has('buku')) {
                foreach ($request->buku as $value) {
                    PengabdianBuku::create([
                        'pengabdian'=>$pengajuan->id,
                        'isbn'=>$value['isbn'],
                        'judul'=>$value['name']
                    ]);
                }
            }

            // Mitra
            if ($request->has('mitra')) {
                foreach ($request->mitra as $value) {
                    PengabdianMitra::create([
                        'pengabdian'=>$pengajuan->id,
                        'mitra'=>$value['id'],
                        'name'=>$value['name'],
                        'alamat'=>$value['alamat'],
                        'product'=>$value['product'],
                        'asset'=>$value['asset'],
                        'omset'=>$value['omset'],
                        'jumlah_karyawan'=>$value['jumlah_karyawan'],
                    ]);
                }
            }

            // Mitra Output
            if ($request->has('mitra_output')) {
                foreach ($request->mitra_output as $value) {
                    PengabdianMitraOutput::create([
                        'pengabdian'=>$pengajuan->id,
                        'mitra_output'=>$value['id'],
                        'name'=>$value['name']
                    ]);
                }
            }

            // Typical
            if ($request->has('typical')) {
                foreach ($request->typical as $value) {
                    PengabdianTypical::create([
                        'pengabdian'=>$pengajuan->id,
                        'typical'=>$value['id'],
                        'name'=>$value['name']
                    ]);
                }
            }


            // Revenue
            if ($request->has('revenue')) {
                foreach ($request->revenue as $value) {
                    PengabdianRevenue::create([
                        'pengabdian'=>$pengajuan->id,
                        'revenues'=>$value['bisnis_unit'],
                        'name'=>$value['name'],
                        'jenis_royalty'=>$value['jenis_royalty'],
                        'jumlah_royalty'=>$value['jumlah_royalty']
                    ]);
                }
            }

            //Add File
            $path = $uploadedFile->store('proposal');
                $data = array(
                    'name' => $uploadedFile->getClientOriginalName(),
                    'hash_name'=> $uploadedFile->hashName(),
                    'extension'=>$extention,
                    'size'=> $uploadedFile->getSize()
                );

            PengabdianProposal::create([
                'pengabdian'=>$pengajuan->id,
                'name'=>$request->judul_proposal,
                'hash_name'=> $uploadedFile->hashName(),
                'directory'=>$path,
                'path'=> '/app'.'/'.$path,
            ]);

            DB::commit();
            return response()->json(['status' => 'success','data'=>$pengajuan]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage(),'data'=>$pengajuan], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            //pengajuan has one dosen dan one revenue jadi masuknya join
            $id = $request->id;
            $allpengajuan = Pengabdian::with([
                // 'anggota'=>function($query){

                //     // $query->select('pengabdian_anggotas.*','mahasiswas.nrp','mahasiswas.name')
                //     // ->leftJoin('mahasiswas','pengabdian_anggotas.anggota','mahasiswas.id')
                //     // ->where('pengabdian_anggotas.role','=',2);

                //     return $query
                //     ->when('pengabdian_anggotas.role'===2,function($q){
                //         return $q->select('pengabdian_anggotas.*','mahasiswas.nrp','mahasiswas.name')
                //         ->leftJoin('mahasiswas','pengabdian_anggotas.anggota','mahasiswas.id');
                //     })
                //     ->when('pengabdian_anggotas.role'===1,function($q){
                //         return $q->select('pengabdian_anggotas.*','dosens.nidn','dosens.name')
                //         ->leftJoin('dosens','pengabdian_anggotas.anggota','dosens.id');
                //     })->get();

                //     //  $data = $query;
                // },
                'dana'=>function($query){
                    return $query->select("pengabdian_pendanaans.id","pengabdian_pendanaans.pengabdian","pengabdian_pendanaans.name","dana_masters.dana")
                    ->leftJoin("dana_masters","dana_masters.id","pengabdian_pendanaans.dana")
                    ->get();
                },
                'iptek'=>function($query){
                    return $query->select("pengabdian_ipteks.id","pengabdian_ipteks.pengabdian","pengabdian_ipteks.name","iptek_masters.iptek")
                    ->leftJoin("iptek_masters","iptek_masters.id","pengabdian_ipteks.iptek")
                    ->get();
                },
                'publikasi'=>function($query){
                    return $query->select("pengabdian_publikasis.id","pengabdian_publikasis.pengabdian","pengabdian_publikasis.name","publikasi_masters.publikasi")
                    ->leftJoin("publikasi_masters","publikasi_masters.id","pengabdian_publikasis.publikasi")
                    ->get();
                },
                'buku'=>function($query){
                    return $query->select("pengabdian_bukus.id","pengabdian_bukus.pengabdian","pengabdian_bukus.isbn","pengabdian_bukus.judul")
                    ->get();
                },
                'mitra'=>function($query){
                    return $query->select("pengabdian_mitras.id","pengabdian_mitras.pengabdian","pengabdian_mitras.name","mitra_masters.mitra")
                    ->leftJoin("mitra_masters","mitra_masters.id","pengabdian_mitras.mitra")
                    ->get();
                },
                'mitra_output'=>function($query){
                    return $query->select("pengabdian_mitra_outputs.id","pengabdian_mitra_outputs.pengabdian","pengabdian_mitra_outputs.name","mitra_outputs.mitra")
                    ->leftJoin("mitra_outputs","mitra_outputs.id","pengabdian_mitra_outputs.mitra_output")
                    ->get();
                },
                'typical'=>function($query){
                    return $query->select("pengabdian_typicals.id","pengabdian_typicals.pengabdian","pengabdian_typicals.name","typicals.typical")
                    ->leftJoin("typicals","typicals.id","pengabdian_typicals.typical")
                    ->get();
                }]);

            $mahasiswa = PengabdianAnggota::select('pengabdian_anggotas.*','mahasiswas.nrp as identitas','mahasiswas.name')
            ->leftJoin('mahasiswas','pengabdian_anggotas.anggota','mahasiswas.id')
            ->where('pengabdian_anggotas.role','=',3)
            ->get();

            $dosen = PengabdianAnggota::select('pengabdian_anggotas.*','dosens.nidn as identitas','dosens.name')
            ->leftJoin('dosens','pengabdian_anggotas.anggota','dosens.id')
            ->where('pengabdian_anggotas.role','=',2)
            ->get();

            $anggota = array();
            foreach ($mahasiswa as $value) {
                array_push($anggota,$value);
            }

            foreach ($dosen as $value) {
                array_push($anggota,$value);
            }
            // array_push($anggota,$mahasiswa);
            // array_push($anggota,$dosen);

            $y = $allpengajuan
            ->leftJoin('pengabdian_approvals AS c','pengabdians.id','c.pengabdian')
            ->leftJoin('dosens','pengabdians.dosen','dosens.id')
            ->leftJoin('kepangkatans','dosens.kepangkatan','kepangkatans.id')
            ->leftJoin('prodis','pengabdians.kode_prodi','prodis.id')
            ->leftJoin('pengabdian_revenues','pengabdian_revenues.pengabdian','pengabdians.id')
            ->leftJoin('unit_bisnis','unit_bisnis.id','pengabdian_revenues.revenues')
            ->select('pengabdians.id','pengabdians.tahun','pengabdians.kode_pengabdian','pengabdians.kode_prodi','pengabdians.total','dosens.name','dosens.nidn','prodis.nama_prodi','kepangkatans.pangkat','unit_bisnis.unit_bisnis','pengabdian_revenues.name as revenue','pengabdian_revenues.jenis_royalty','pengabdian_revenues.jumlah_royalty','c.status')
            ->where('pengabdians.id',$id)
            ->first();

            $z = PengabdianProposal::where('pengabdian',$id)->first();
            $proposal = url('/').'/api/'.$z->directory;

            $data = array(
                        'id'=>$y->id,
                        'kode_pengabdian'=>$y->kode_pengabdian,
                        'status'=>$y->status,
                        'tahun'=>$y->tahun,
                        'kode_prodi'=>$y->kode_prodi,
                        'total'=>$y->total,
                        'name'=>$y->name,
                        'nidn'=>$y->nidn,
                        'nama_prodi'=>$y->nama_prodi,
                        'pangkat'=>$y->pangkat,
                        'unit_bisnis'=>$y->unit_bisnis,
                        'revenue'=>$y->revenue,
                        'jenis_royalty'=>$y->jenis_royalty,
                        'jumlah_royalty'=>$y->jumlah_royalty,
                        'anggota'=>$anggota,
                        'dana'=>$y->dana,
                        'iptek'=>$y->iptek,
                        'publikasi'=>$y->publikasi,
                        'buku'=>$y->buku,
                        'mitra'=>$y->mitra,
                        'mitra_output'=>$y->mitra_output,
                        'typical'=>$y->typical,
                        'proposal'=>$proposal
                );

            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function export($name)
    {
        $path = DB::table('pengabdian_proposals')
        ->select('path')
        ->where('hash_name',$name)
        ->first();

        // return Storage::download($path->path);
        $data = storage_path($path->path);
        return response()->file($data);
    }

    public function generateCode(Request $request)
    {
        try {
            $data = Pengabdian::where('dosen',$request->dosen)->count();
            $data = $data + 1;
            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
