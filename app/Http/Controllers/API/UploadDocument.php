<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadDocument extends Controller
{
    public function create(Request $request)
    {
        try {
            $uploadedFile = $request->file('file');
            $extention = $uploadedFile->extension();
            if ($extention !== "pdf") {
                return response()->json(['status' => 'failed','error' => 'file not supported'],500);
            } else {
                $path = $uploadedFile->store('proposal');
                $data = array(
                    'name' => $uploadedFile->getClientOriginalName(),
                    'hash_name'=> $uploadedFile->hashName(),
                    'extension'=>$extention,
                    'size'=> $uploadedFile->getSize()
                );
            }
            $test = array();
            foreach ($request->items as $value) {
                array_push($test,$value);
            }
            $data = array(
                'name'=>$request->name,
                'item'=> $test
            );


            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }

    }
}
