<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PendidikanDosen;
use DB;

class PendidikanDosenController extends Controller
{

    public function create(Request $request)
    {
        try {
            $mitra = PendidikanDosen::create([
                'dosen' => $request->dosen,
                'pendidikan' => $request->pendidikan,
                'univ' => $request->univ,
                'jurusan' => $request->jurusan,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $mitra = MitraMaster::find($id);
            $mitra->update([
                'mitra' => $request->mitra,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $mitra = MitraMaster::find($id);
            $mitra->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
