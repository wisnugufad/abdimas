<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengurus;

class PengurusController extends Controller
{
    public function index()
    {
        try {
            $dana = Pengurus::select('*')->first();
            return response()->json(['status' => 'success','data'=>$dana]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {
        try {
            $publikasi = Pengurus::find($id);
            $publikasi->update([
                'name' => $request->name,
                'nidn' => $request->nidn,
                'email' => $request->email,
                'telp' => $request->telp,
                'alamat' => $request->alamat
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
