<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengabdian;
use App\Models\PengabdianAnggota;
use App\Models\Dosen;
use DB;

class SuratController extends Controller
{
    public function surat_tugas()
    {
        try {

            $prodi = request()->prodi;
            $semester = request()->semester;

            $data_ketua = DB::table('pengabdians AS a')
            ->leftJoin('pengabdian_pendanaans AS b','a.id','b.pengabdian')
            ->leftJoin('dana_masters AS e','e.id','b.dana')
            ->leftJoin('pengabdian_mitras AS c','a.id','c.pengabdian')
            ->leftJoin('dosens AS d','d.id','a.dosen')
            ->where('a.kode_prodi',$prodi)
            ->where('a.semester',$semester)
            ->select(
                'a.judul','a.bidang','d.name','e.dana','c.name as mitra','b.jumlah'
            )->get();

            $rekap = array();
            foreach ($data_ketua as $value) {
                $ketua = array(
                    'judul'=>$value->judul,
                    'bidang'=>$value->bidang,
                    'name'=>$value->name,
                    'dana'=>$value->dana,
                    'jabatan'=>'KETUA',
                    'keterlibatan'=>$value->mitra,
                    'jumlah'=>$value->jumlah,
                    'SKA'=>'',
                    'mahasiswa'=>''
                );
                array_push($rekap,$ketua);
            }

            $data_anggota=DB::table('pengabdians AS a')
            ->leftJoin('pengabdian_pendanaans AS b','a.id','b.pengabdian')
            ->leftJoin('dana_masters AS e','e.id','b.dana')
            ->leftJoin('pengabdian_anggotas AS f','a.id','f.pengabdian')
            ->leftJoin('pengabdian_mitras AS c','a.id','c.pengabdian')
            ->leftJoin('dosens AS d','d.id','f.anggota')
            ->where('a.kode_prodi',$prodi)
            ->where('a.semester',$semester)
            ->where('f.role',2)
            ->select(
                'a.judul','a.bidang','d.name','e.dana','c.name as mitra','b.jumlah'
            )->get();

            foreach ($data_anggota as $value) {
                $anggota = array(
                    'judul'=>$value->judul,
                    'bidang'=>$value->bidang,
                    'name'=>$value->name,
                    'dana'=>$value->dana,
                    'jabatan'=>'ANGGOTA',
                    'keterlibatan'=>$value->mitra,
                    'jumlah'=>$value->jumlah,
                    'SKA'=>'',
                    'mahasiswa'=>''
                );
                array_push($rekap,$anggota);
            }

            return response()->json(['status' => 'success','data'=>$rekap]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function surat_pengesahan()
    {
        try {
            $kode = request()->kode;
            $allpengajuan = Pengabdian::with([
                'mitra'=>function($query){
                    return $query->select("pengabdian_mitras.id","pengabdian_mitras.pengabdian","pengabdian_mitras.name","mitra_masters.mitra","pengabdian_mitras.alamat")
                    ->leftJoin("mitra_masters","mitra_masters.id","pengabdian_mitras.mitra")
                    ->get();
                },
                'dana'=>function($query){
                    return $query->select("pengabdian_pendanaans.id","pengabdian_pendanaans.pengabdian","pengabdian_pendanaans.name","dana_masters.dana","pengabdian_pendanaans.jumlah")
                    ->leftJoin("dana_masters","dana_masters.id","pengabdian_pendanaans.dana")
                    ->get();
                }
                ])
                ->where('pengabdians.kode_pengabdian',$kode);

            $mahasiswa = PengabdianAnggota::select('pengabdian_anggotas.role','mahasiswas.*','mahasiswas.name','prodis.nama_prodi as prodi')
            ->leftJoin('pengabdians','pengabdians.id','pengabdian_anggotas.pengabdian')
            ->leftJoin('mahasiswas','pengabdian_anggotas.anggota','mahasiswas.id')
            ->leftJoin('prodis','prodis.id','mahasiswas.prodi')
            ->where('pengabdians.kode_pengabdian',$kode)
            ->where('pengabdian_anggotas.role','=',3)
            ->get();

            $dosen = PengabdianAnggota::leftJoin('dosens','pengabdian_anggotas.anggota','dosens.id')
            ->leftJoin('pengabdians','pengabdians.id','pengabdian_anggotas.pengabdian')
            ->leftJoin('kepangkatans','dosens.kepangkatan','kepangkatans.id')
            ->leftJoin('prodis','dosens.prodi','prodis.id')
            ->where('pengabdians.kode_pengabdian',$kode)
            ->where('pengabdian_anggotas.role','=',2)
            ->select('pengabdian_anggotas.role','dosens.*','kepangkatans.pangkat','prodis.nama_prodi as prodi')
            ->get();

            $anggota = array();
            foreach ($mahasiswa as $value) {
                array_push($anggota,$value);
            }

            foreach ($dosen as $value) {
                array_push($anggota,$value);
            }

            $y = $allpengajuan
            ->leftJoin('dosens','pengabdians.dosen','dosens.id')
            ->leftJoin('kepangkatans','dosens.kepangkatan','kepangkatans.id')
            ->leftJoin('semesters','pengabdians.semester','semesters.id')
            ->leftJoin('prodis','dosens.prodi','prodis.id')
            ->select('pengabdians.*','dosens.name','dosens.nidn','dosens.gelar','dosens.email','dosens.telp','kepangkatans.pangkat','semesters.semester','prodis.nama_prodi')
            ->first();

            $data = array(
                'id'=>$y->id,
                'kode_pengabdian'=>$y->kode_pengabdian,
                'judul'=>$y->judul,
                'semester'=>$y->semester,
                'tahun'=>$y->tahun,
                'bidang'=>$y->bidang,
                'total'=>$y->total,
                'name'=>$y->name.','.$y->gelar,
                'nidn'=>$y->nidn,
                'email'=>$y->email,
                'telp'=>$y->telp,
                'nama_prodi'=>$y->nama_prodi,
                'pangkat'=>$y->pangkat,
                'anggota'=>$anggota,
                'mitra'=>$y->mitra,
                'dana'=>$y->dana
            );

            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
