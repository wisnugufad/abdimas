<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Typical;
use App\Http\Resources\TypicalCollection;

class TypicalController extends Controller
{
    public function index()
    {
        try {
            $data = Typical::orderBy('id','ASC');
            $keyword = request()->search;

		if (is_null($keyword)) {
                $typical = $data;
            } else {
                $typical = $data->where('typicals.typical','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;

            return new TypicalCollection($typical->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $typical = Typical::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$typical]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $typical = Typical::create([
                'typical' => $request->typical,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $typical = Typical::find($id);
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $typical = Typical::find($id);
            $typical->update([
                'typical' => $request->typical,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $typical = Typical::find($id);
            $typical->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
