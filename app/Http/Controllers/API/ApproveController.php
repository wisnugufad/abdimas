<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengabdian;
use App\Models\PengabdianApproval;
use App\Http\Resources\ApproveCollection;
use DB;

class ApproveController extends Controller
{
    public function index(Request $request)
    {
        try {

            $role = $request->role;
            $status = $request->status;
            $user = $request->user;
            $limit = $request->limit;

            $data = DB::table("pengabdians AS a")
            ->join('pengabdian_approvals AS b','a.id','b.pengabdian')
            ->join('dosens AS c','c.id','a.dosen')
            ->join('prodis AS d','d.id','a.kode_prodi')
            ->select('a.id','a.kode_pengabdian','a.judul','a.tahun','b.status','c.name','d.nama_prodi')
            ->where('b.status',$status);

            if ($role === 2 || $role === "2") {
                $data2 = $data->where("a.dosen",$user);
            } else {
                $data2 = $data;
            }

            // return response()->json(['status' => 'success','data' => $data2],200);
            return new ApproveCollection($data2->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
       }

    }

    public function count()
    {
        try {
            $data = DB::table('pengabdian_approvals')
            ->where('status','SUBMITED')
            ->count();

            return response()->json(['status' => 'success','data' => $data],200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
       }
    }

    public function update(Request $request)
    {
        try {
            $data = PengabdianApproval::find($request->id);
            $data->update([
                'status'=>$request->status
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
       }
    }
}
