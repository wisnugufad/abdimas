<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DanaMaster;
use App\Http\Resources\DanaCollection;

class DanaController extends Controller
{
    public function index()
    {
        try {

            $keyword = request()->search;

            $data = DanaMaster::orderBy('id','ASC');

            if (is_null($keyword)) {
                $dana = $data;
            } else {
                $dana = $data->where('dana_masters.dana','ILIKE','%'.$keyword.'%');
            }
            $limit = request()->limit;
            return new DanaCollection($dana->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $dana = DanaMaster::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$dana]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $dana = DanaMaster::create([
                'dana' => $request->dana,
                'bobot' => $request->bobot,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $dana = DanaMaster::find($id);
            return response()->json(['status' => 'success','data'=>$dana]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $dana = DanaMaster::find($id);
            $dana->update([
                'dana' => $request->dana,
                'bobot' => $request->bobot,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $dana = DanaMaster::find($id);
            $dana->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
