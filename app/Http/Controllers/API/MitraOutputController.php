<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MitraOutput;
use App\Http\Resources\MitraOutputCollection;

class MitraOutputController extends Controller
{
    public function index()
    {
        try {
            $data = MitraOutput::orderBy('id','ASC');
            $keyword = request()->search;

		    if (is_null($keyword)) {
                $mitra = $data;
            } else {
                $mitra = $data->where('mitra_outputs.mitra','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;

            return new MitraOutputCollection($mitra->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $mitra = MitraOutput::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $mitra = MitraOutput::create([
                'mitra' => $request->mitra,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $mitra = MitraOutput::find($id);
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $mitra = MitraOutput::find($id);
            $mitra->update([
                'mitra' => $request->mitra,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $mitra = MitraOutput::find($id);
            $mitra->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
