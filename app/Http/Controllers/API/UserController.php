<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Dosen;
use DB;

class UserController extends Controller
{
    public function register(Request $request)
    {
       try {
           DB::beginTransaction();
           $data = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'role'=>$request->role,
            'email_verified_at'=>now(),
            'password' => bcrypt($request->password),
           ]);

           Dosen::create([
               'user'=>$data->id,
               'nidn'=>$request->email,
               'name'=>$request->name
           ]);

           DB::commit();
           return response()->json(['status' => 'success','data' => $data],200);
       } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
       }
    }
}
