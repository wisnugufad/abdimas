<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Models\Dosen;
use App\Models\PendidikanDosen;
use App\Http\Resources\DosenCollection;
use DB;

class DosenController extends Controller
{
    public function index()
    {
        try {
            $data = Dosen::select('dosens.id','dosens.user','dosens.nidn','dosens.prodi','dosens.name','dosens.gelar','dosens.kepangkatan','b.pangkat','dosens.telp')
            ->leftJoin('kepangkatans AS b','b.id','dosens.kepangkatan')
            ->with(['pendidikan'=>function($query)
            {
                return $query->select('pendidikan_dosens.id','pendidikan_dosens.dosen','c.tingkat','pendidikan_dosens.univ','pendidikan_dosens.jurusan')
                ->join('pendidikan_masters AS c','pendidikan_dosens.pendidikan','c.id');
            }
            ])->orderBy('dosens.id','ASC');


            $keyword = request()->keyword;
            if ($keyword !== '' && $keyword !== "undefined") {
                $dosen = $data->where('dosens.nidn','ILIKE','%'.$keyword.'%')
                ->orWhere('dosens.name','ILIKE','%'.$keyword.'%');
            }else{
                $dosen = $data;
            }

            // $data = Dosen::select('password')->where('id',1)->first();
            // $dosen = Hash::check('iti123',$data->password);

            $limit = request()->limit;
            return new DosenCollection($dosen->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {

            $dosen = Dosen::select('dosens.id','dosens.nidn','dosens.prodi','dosens.name','dosens.kepangkatan','b.pangkat','dosens.telp')
            ->join('kepangkatans AS b','b.id','dosens.kepangkatan')
            ->with(['pendidikan'=>function($query)
            {
                return $query->select('pendidikan_dosens.id','pendidikan_dosens.dosen','c.tingkat','pendidikan_dosens.univ','pendidikan_dosens.jurusan')
                ->join('pendidikan_masters AS c','pendidikan_dosens.pendidikan','c.id');
            }
            ])
            ->orderBy('dosens.id','ASC');

            $search = request()->keyword;
            if (is_null($search)) {
                $dropdown = $dosen->get();
            } else {
                $dropdown = $dosen->where('dosens.nidn','ILIKE','%'.$search.'%')
                ->orWhere('dosens.name','ILIKE','%'.$search.'%')->get();
            }

            // $data = Dosen::select('password')->where('id',1)->first();
            // $dosen = Hash::check('iti123',$data->password);

            return response()->json(['status' => 'success','data'=>$dropdown]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {

            $dosen = Dosen::select('dosens.id','dosens.nidn','dosens.prodi','dosens.name','b.pangkat','dosens.telp')
            ->join('kepangkatans AS b','b.id','dosens.kepangkatan')
            ->with(['pendidikan'=>function($query)
            {
                return $query->select('pendidikan_dosens.id','pendidikan_dosens.dosen','c.tingkat','pendidikan_dosens.univ','pendidikan_dosens.jurusan')
                ->join('pendidikan_masters AS c','pendidikan_dosens.pendidikan','c.id');
            }
            ])
            ->orderBy('dosens.id','ASC')
            ->where('dosens.id',$id)
            ->first();

            // $data = Dosen::select('password')->where('id',1)->first();
            // $dosen = Hash::check('iti123',$data->password);

            return response()->json(['status' => 'success','data'=>$dosen]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function profil($id)
    {
        try {

            $dosen = Dosen::select('dosens.*','b.pangkat')
            ->join('kepangkatans AS b','b.id','dosens.kepangkatan')
            ->with(['pendidikan'=>function($query)
            {
                return $query->select('pendidikan_dosens.id','pendidikan_dosens.dosen','c.id as tingkat_id','c.tingkat','pendidikan_dosens.univ','pendidikan_dosens.jurusan')
                ->join('pendidikan_masters AS c','pendidikan_dosens.pendidikan','c.id');
            }
            ])
            ->orderBy('dosens.id','ASC')
            ->where('dosens.user',$id)
            ->first();

            // $data = Dosen::select('password')->where('id',1)->first();
            // $dosen = Hash::check('iti123',$data->password);

            return response()->json(['status' => 'success','data'=>$dosen]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            try {
                DB::beginTransaction();
                $data = User::create([
                 'name'=>$request->name,
                 'email'=>$request->email,
                 'role'=>$request->role,
                 'email_verified_at'=>now(),
                 'password' => bcrypt($request->password),
                ]);

                Dosen::create([
                    'user'=>$data->id,
                    'nidn'=>$request->nidn,
                    'prodi' => $request->prodi,
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'telp' => $request->telp,
                    'gelar'=>$request->gelar,
                    'tgl_lahir'=>$request->tgl_lahir,
                    'kepangkatan'=>$request->pangkat,
                    'alamat'=>$request->alamat
                ]);

                DB::commit();
                return response()->json(['status' => 'success','data' => $data],200);
            } catch (\Exception $e) {
                 DB::rollback();
                 return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    // public function find($id)
    // {
    //     try {
    //         $dana = Dosen::find($id)
    //         ->join('kepangkatans AS b','b.id','dosens.kepangkatan')
    //         ->with(['pendidikan']);
    //         return response()->json(['status' => 'success','data'=>$dana]);
    //     } catch (\Exception $e) {
    //         return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
    //     }
    // }

    public function update(Request $request,$id)
    {

        try {
            $dana = Dosen::find($id);
            $dana->update([
                'nidn' => $request->nidn,
                'prodi' => $request->prodi,
                'name' => $request->name,
                'gelar'=>$request->gelar,
                'tgl_lahir'=>$request->tgl_lahir,
                'email'=>$request->email,
                'telp' => $request->telp,
                'alamat'=>$request->alamat,
                'kepangkatan'=>$request->kepangkatan
            ]);

            // foreach ($request->pendidikan as $value) {
            //     $pendidikan = PendidikanDosen::find($value['id']);
            //     $pendidikan->update([
            //         'dosen' => $value['dosen'],
            //         'pendidikan' => $value['tingkat_id'],
            //         'univ' => $value['univ'],
            //         'jurusan' => $value['jurusan']
            //     ]);
            // }

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    // public function hapus($id)
    // {
    //     try {
    //         $dana = DanaMaster::find($id);
    //         $dana->delete();

    //         return response()->json(['status' => 'success']);
    //     } catch (\Exception $e) {
    //         return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
    //     }
    // }
}
