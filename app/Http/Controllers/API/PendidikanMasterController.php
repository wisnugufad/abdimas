<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PendidikanMaster;
use App\Http\Resources\PendidikanMasterCollection;

class PendidikanMasterController extends Controller
{
    public function index()
    {
        try {
            $keyword = request()->search;

            $data = PendidikanMaster::orderBy('id','ASC');

            if (is_null($keyword) || $keyword === 'undefined') {
                $pendidikan = $data;
            } else {
                $pendidikan = $data->where('pendidikan_masters.tingkat','ILIKE','%'.$keyword.'%')
                ->orWhere('pendidikan_masters.pendidikan','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;
            return new PendidikanMasterCollection($pendidikan->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $pendidikan = PendidikanMaster::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$pendidikan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $pendidikan = PendidikanMaster::create([
                'tingkat' => $request->tingkat,
                'pendidikan' => $request->pendidikan,
                'description' => $request->description,
            ]);
            return response()->json(['status' => 'success','data'=>$pendidikan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $pendidikan = PendidikanMaster::find($id);
            return response()->json(['status' => 'success','data'=>$pendidikan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {
        try {
            $pendidikan = PendidikanMaster::find($id);
            $pendidikan->update([
                'tingkat' => $request->tingkat,
                'pendidikan' => $request->pendidikan,
                'description' => $request->description,
            ]);
            return response()->json(['status' => 'success','data'=>$pendidikan]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $pendidikan = PendidikanMaster::find($id);
            $pendidikan->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
