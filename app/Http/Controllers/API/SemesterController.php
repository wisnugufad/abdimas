<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Semester;
use App\Http\Resources\SemesterCollection;
use DB;

class SemesterController extends Controller
{
    public function index()
    {
        try {
            $data = Semester::orderBy('id','ASC');
            $keyword = request()->search;

		if (is_null($keyword) || $keyword=="undefined") {
                $publikasi = $data;
            } else {
                $publikasi = $data->where('semesters.semester','ILIKE','%'.$keyword.'%');
            }
            $limit = request()->limit;
            return new SemesterCollection($publikasi->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $dana = Semester::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$dana]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $publikasi = Semester::create([
                'semester' => $request->semester
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $publikasi = Semester::find($id);
            return response()->json(['status' => 'success','data'=>$publikasi]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $publikasi = Semester::find($id);
            $publikasi->update([
                'semester' => $request->semester
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $publikasi = Semester::find($id);
            $publikasi->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
