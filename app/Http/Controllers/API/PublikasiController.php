<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PublikasiMaster;
use App\Http\Resources\PublikasiCollection;

class PublikasiController extends Controller
{
    public function index()
    {
        try {
            $data = PublikasiMaster::orderBy('id','ASC');
            $keyword = request()->search;

		if (is_null($keyword)) {
                $publikasi = $data;
            } else {
                $publikasi = $data->where('publikasi_masters.publikasi','ILIKE','%'.$keyword.'%');
            }
            $limit = request()->limit;
            return new PublikasiCollection($publikasi->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $dana = PublikasiMaster::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$dana]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $publikasi = PublikasiMaster::create([
                'publikasi' => $request->publikasi,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $publikasi = PublikasiMaster::find($id);
            return response()->json(['status' => 'success','data'=>$publikasi]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $publikasi = PublikasiMaster::find($id);
            $publikasi->update([
                'publikasi' => $request->publikasi,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $publikasi = PublikasiMaster::find($id);
            $publikasi->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
