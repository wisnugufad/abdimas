<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UnitBisnis;
use App\Http\Resources\UnitBisnisCollection;

class UnitBisnisController extends Controller
{
    public function index()
    {
        try {
            $data = UnitBisnis::orderBy('id','ASC');
            $keyword = request()->search;

		if (is_null($keyword)) {
                $unit = $data;
            } else {
                $unit = $data->where('unit_bisnis.unit_bisnis','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;

            return new UnitBisnisCollection($unit->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $unit = UnitBisnis::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$unit]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $unit = UnitBisnis::create([
                'unit_bisnis' => $request->unit_bisnis,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $unit = UnitBisnis::find($id);
            return response()->json(['status' => 'success','data'=>$unit]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $unit = UnitBisnis::find($id);
            $unit->update([
                'unit_bisnis' => $request->unit_bisnis,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $unit = UnitBisnis::find($id);
            $unit->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
