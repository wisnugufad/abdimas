<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MitraMaster;
use App\Http\Resources\MitraCollection;

class MitraController extends Controller
{
    public function index()
    {
        try {
            $keyword = request()->search;

            $data = MitraMaster::orderBy('id','ASC');

            if (is_null($keyword)) {
                $mitra = $data;
            } else {
                $mitra = $data->where('mitra_masters.mitra','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;
            return new MitraCollection($mitra->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $mitra = MitraMaster::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $mitra = MitraMaster::create([
                'mitra' => $request->mitra,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $mitra = MitraMaster::find($id);
            return response()->json(['status' => 'success','data'=>$mitra]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $mitra = MitraMaster::find($id);
            $mitra->update([
                'mitra' => $request->mitra,
                'bobot' => $request->bobot
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $mitra = MitraMaster::find($id);
            $mitra->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
