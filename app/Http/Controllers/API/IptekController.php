<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IptekMaster;
use App\Http\Resources\IptekCollection;

class IptekController extends Controller
{
    public function index()
    {
        try {
            $data = IptekMaster::orderBy('id','ASC');
            $keyword = request()->search;

		    if (is_null($keyword)) {
                $iptek = $data;
            } else {
                $iptek = $data->where('iptek_masters.iptek','ILIKE','%'.$keyword.'%');
            }
            $limit=request()->limit;

            return new IptekCollection($iptek->paginate($limit));
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function dropdown()
    {
        try {
            $iptek = IptekMaster::orderBy('id','ASC')->get();
            return response()->json(['status' => 'success','data'=>$iptek]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function create(Request $request)
    {
        try {
            $iptek = IptekMaster::create([
                'iptek' => $request->iptek,
                'bobot' => $request->bobot,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function find($id)
    {
        try {
            $iptek = IptekMaster::find($id);
            return response()->json(['status' => 'success','data'=>$iptek]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function update(Request $request,$id)
    {

        try {
            $iptek = IptekMaster::find($id);
            $iptek->update([
                'iptek' => $request->iptek,
                'bobot' => $request->bobot,
            ]);

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }

    public function hapus($id)
    {
        try {
            $iptek = IptekMaster::find($id);
            $iptek->delete();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed','error' => $e->getMessage()],500);
        }
    }
}
