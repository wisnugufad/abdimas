<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Auth\LoginController@login');
Route::post('/register', 'API\UserController@register');
Route::post('/auth', 'Auth\LoginController@auth');
// test api
Route::post('/upload', 'API\UploadDocument@create');

// API Prodi
Route::get('prodi','API\ProdiController@index');
Route::get('prodi-dropdown','API\ProdiController@dropdown');
Route::post('prodi/add','API\ProdiController@create');
Route::get('prodi/{id}','API\ProdiController@find');
Route::put('prodi/{id}','API\ProdiController@update');
Route::delete('prodi/{id}','API\ProdiController@hapus');

// API Pendidikan
Route::get('pendidikan','API\PendidikanMasterController@index');
Route::get('pendidikan-dropdown','API\PendidikanMasterController@dropdown');
Route::post('pendidikan/add','API\PendidikanMasterController@create');
Route::get('pendidikan/{id}','API\PendidikanMasterController@find');
Route::put('pendidikan/{id}','API\PendidikanMasterController@update');
Route::delete('pendidikan/{id}','API\PendidikanMasterController@hapus');

// API Kepangkatan
Route::get('kepangkatan','API\KepangkatanController@index');
Route::get('kepangkatan-dropdown','API\KepangkatanController@dropdown');
Route::post('kepangkatan/add','API\KepangkatanController@create');
Route::get('kepangkatan/{id}','API\KepangkatanController@find');
Route::put('kepangkatan/{id}','API\KepangkatanController@update');
Route::delete('kepangkatan/{id}','API\KepangkatanController@hapus');

// API Dana
Route::get('dana','API\DanaController@index');
Route::get('dana-dropdown','API\DanaController@dropdown');
Route::post('dana/add','API\DanaController@create');
Route::get('dana/{id}','API\DanaController@find');
Route::put('dana/{id}','API\DanaController@update');
Route::delete('dana/{id}','API\DanaController@hapus');

// API Iptek
Route::get('iptek','API\IptekController@index');
Route::get('iptek-dropdown','API\IptekController@dropdown');
Route::post('iptek/add','API\IptekController@create');
Route::get('iptek/{id}','API\IptekController@find');
Route::put('iptek/{id}','API\IptekController@update');
Route::delete('iptek/{id}','API\IptekController@hapus');

// API Publikasi
Route::get('publikasi','API\PublikasiController@index');
Route::get('publikasi-dropdown','API\PublikasiController@dropdown');
Route::post('publikasi/add','API\PublikasiController@create');
Route::get('publikasi/{id}','API\PublikasiController@find');
Route::put('publikasi/{id}','API\PublikasiController@update');
Route::delete('publikasi/{id}','API\PublikasiController@hapus');

// API Mitra
Route::get('mitra','API\MitraController@index');
Route::get('mitra-dropdown','API\MitraController@dropdown');
Route::post('mitra/add','API\MitraController@create');
Route::get('mitra/{id}','API\MitraController@find');
Route::put('mitra/{id}','API\MitraController@update');
Route::delete('mitra/{id}','API\MitraController@hapus');

// API Mitra Output
Route::get('mitra-output','API\MitraOutputController@index');
Route::get('mitra-output-dropdown','API\MitraOutputController@dropdown');
Route::post('mitra-output/add','API\MitraOutputController@create');
Route::get('mitra-output/{id}','API\MitraOutputController@find');
Route::put('mitra-output/{id}','API\MitraOutputController@update');
Route::delete('mitra-output/{id}','API\MitraOutputController@hapus');

// API Typical
Route::get('typical','API\TypicalController@index');
Route::get('typical-dropdown','API\TypicalController@dropdown');
Route::post('typical/add','API\TypicalController@create');
Route::get('typical/{id}','API\TypicalController@find');
Route::put('typical/{id}','API\TypicalController@update');
Route::delete('typical/{id}','API\TypicalController@hapus');

// API Unit Bisnis
Route::get('unit-bisnis','API\UnitBisnisController@index');
Route::get('unit-bisnis-dropdown','API\UnitBisnisController@dropdown');
Route::post('unit-bisnis/add','API\UnitBisnisController@create');
Route::get('unit-bisnis/{id}','API\UnitBisnisController@find');
Route::put('unit-bisnis/{id}','API\UnitBisnisController@update');
Route::delete('unit-bisnis/{id}','API\UnitBisnisController@hapus');

// API Dosen
Route::get('dosen','API\DosenController@index');
Route::get('dosen-dropdown','API\DosenController@dropdown');
Route::get('dosen/profil/{id}','API\DosenController@profil');
Route::get('dosen/{id}','API\DosenController@find');
Route::post('dosen/add','API\DosenController@create');
Route::put('dosen/{id}','API\DosenController@update');

// API Mahasiswa
Route::get('mahasiswa','API\MahasiswaController@index');
Route::get('mahasiswa-dropdown','API\MahasiswaController@dropdown');
Route::post('mahasiswa/add','API\MahasiswaController@create');
Route::get('mahasiswa/{id}','API\MahasiswaController@find');
Route::put('mahasiswa/{id}','API\MahasiswaController@update');
Route::delete('mahasiswa/{id}','API\MahasiswaController@hapus');

// API Pengajuan
Route::get('pengajuan','API\PengajuanController@index');
Route::get('pengajuan/view/{id}','API\PengajuanController@view');
Route::post('pengajuan/add','API\PengajuanController@create');
Route::get('count','API\PengajuanController@generateCode');

// API Semester
Route::get('semester','API\SemesterController@index');
Route::get('semester-dropdown','API\SemesterController@dropdown');
Route::post('semester/add','API\SemesterController@create');
Route::get('semester/{id}','API\SemesterController@find');
Route::put('semester/{id}','API\SemesterController@update');
Route::delete('semester/{id}','API\SemesterController@hapus');

Route::get('proposal/{name}','API\PengajuanController@export');

// API Approval
Route::get('approval','API\ApproveController@index');
Route::get('approval/count','API\ApproveController@count');
Route::put('approval/{id}','API\ApproveController@update');

// API Semester
Route::get('pengurus','API\PengurusController@index');
Route::put('pengurus/{id}','API\PengurusController@update');

//API Pendidikan Dosen
Route::post('pend-dosen/add','API\PendidikanDosenController@create');

// Api Dashboard
Route::get('dashboard','API\DashboardController@index');

//API SURAT
Route::get('surat/tugas','API\SuratController@surat_tugas');
Route::get('surat/pengesahan','API\SuratController@surat_pengesahan');
