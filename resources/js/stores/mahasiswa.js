import $axios from '../api.js'

const state = () => ({
    mahasiswas: [],
    ddMahasiswas: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.mahasiswas = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddMahasiswas = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getMahasiswas({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/mahasiswa?page=${search.page}&limit=${search.limit}&keyword=${search.keyword}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    ddMahasiswas({
        commit,
        state
    }, payload) {
        return new Promise((resolve, reject) => {
            let search = typeof payload != 'undefined' ? payload : ''
            $axios.get(`/mahasiswa-dropdown?keyword=${search}`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addMahasiswas({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/mahasiswa/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updateMahasiswas({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.put(`/mahasiswa/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deleteMahasiswas({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.delete(`/mahasiswa/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
