import $axios from '../api.js'

const state = () => ({
    pengajuans: [],
    ddProdis: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.pengajuans = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddProdis = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getPengajuans({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.get(`/pengajuan?page=${search.page}&limit=${search.limit}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    generateCodePengajuan({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/count?dosen=${search}`)
                .then((response) => {
                    // commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    viewPengajuans({
        commit,
        state
    }, payload) {
        let id = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.get(`/pengajuan/view/${id}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    ddPengajuans({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.get(`/prodi-dropdown`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addPengajuans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.post(`/pengajuan/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updatePengajuans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.put(`/prodi/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deletePengajuans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pengajuan?page=${state.page}&q=${search}`)
            $axios.delete(`/prodi/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
