import $axios from '../api.js'

const state = () => ({
    approves: [],
    count: 0,
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.approves = payload
    },
    ASSIGN_COUNT(state, payload) {
        state.count = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getApprove({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/approval?page=${search.page}&limit=${search.limit}&role=${search.role}&user=${search.user}&status=${search.status}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    updateApproves({
        commit,
        state
    }, payload) {
        return new Promise((resolve, reject) => {
            $axios.put(`/approval/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    getCount({
        commit,
        state
    }) {
        // let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/approval/count`)
                .then((response) => {
                    // commit('ASSIGN_COUNT', response.data.data)
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
