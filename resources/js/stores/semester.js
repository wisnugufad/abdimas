import $axios from '../api.js'

const state = () => ({
    semesters: [],
    ddSemesters: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.semesters = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddSemesters = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getSemesters({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/semester?search=${search.keyword}&page=${search.page}&limit=${search.limit}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    ddSemesters({
        commit,
        state
    }) {
        return new Promise((resolve, reject) => {
            $axios.get(`/semester-dropdown`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addSemesters({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/semester/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updateSemesters({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.put(`/semester/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deleteSemesters({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.delete(`/semester/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
