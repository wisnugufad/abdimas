import $axios from '../api.js'

const state = () => ({
    pendidikans: [],
    ddPendidikans: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.pendidikans = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddPendidikans = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getPendidikans({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/pendidikan?search=${search.keyword}&page=${search.page}&limit=${search.limit}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    ddPendidikans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/pendidikan-dropdown`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addPendidikans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/pendidikan/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    addPendidikanDosens({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/pend-dosen/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updatePendidikans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.put(`/pendidikan/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deletePendidikans({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.delete(`/pendidikan/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
