import $axios from '../api.js'

const state = () => ({
    dosens: [],
    ddDosens: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.dosens = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddDosens = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getDosens({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/dosen?page=${search.page}&limit=${search.limit}&keyword=${search.keyword}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    ddDosens({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/dosen-dropdown?keyword=${search}`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    findDosens({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/dosen/${search}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    profilDosens({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/dosen/profil/${search}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addDosens({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/dosen/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updateDosens({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.put(`/dosen/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deleteDosens({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.delete(`/dosen/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
