import $axios from '../api.js'

const state = () => ({
    userId: "",
    username: "",
    role: ""
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.userId = payload.id
        state.username = payload.name
        state.role = payload.role
    },
}

const actions = {
    submit({
        commit
    }, payload) {
        localStorage.setItem('token', null) //RESET LOCAL STORAGE MENJADI NULL
        commit('SET_TOKEN', null, {
            root: true
        }) //RESET STATE TOKEN MENJADI NULL
        //KARENA MUTATIONS SET_TOKEN BERADA PADA ROOT STORES, MAKA DITAMBAHKAN PARAMETER
        //{ root: true }

        //KITA MENGGUNAKAN PROMISE AGAR FUNGSI SELANJUTNYA BERJALAN KETIKA FUNGSI INI SELESAI
        return new Promise((resolve, reject) => {
            //MENGIRIM REQUEST KE SERVER DENGAN URI /login
            //DAN PAYLOAD ADALAH DATA YANG DIKIRIMKAN DARI COMPONENT LOGIN.VUE
            $axios.post('/login', payload)
                .then((response) => {
                    //KEMUDIAN JIKA RESPONNYA SUKSES
                    if (response.data.status == 'success') {
                        //MAKA LOCAL STORAGE DAN STATE TOKEN AKAN DISET MENGGUNAKAN
                        //API DARI SERVER RESPONSE
                        commit('ASSIGN_DATA', response.data.data)
                        console.log(response.data.data)
                        localStorage.setItem('token', response.data.data.token)
                        commit('SET_TOKEN', response.data.data.token, {
                            root: true
                        })
                    } else {
                        commit('SET_ERRORS', {
                            invalid: 'Email/Password Salah'
                        }, {
                            root: true
                        })
                    }
                    //JANGAN LUPA UNTUK MELAKUKAN RESOLVE AGAR DIANGGAP SELESAI
                    resolve(response.data)
                })
                .catch((error) => {
                    if (error.response.status == 422) {
                        commit('SET_ERRORS', error.response.data.errors, {
                            root: true
                        })
                    }
                })
        })
    },

    auth({
        commit,
        state
    }, payload) {
        return new Promise((resolve, reject) => {
            $axios.post(`/auth`, payload)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    // state.userId = response.data.data.id
                    // state.username = response.data.data.name
                    // state.role = response.data.data.role
                    resolve(response.data.data)
                    commit('SET_USER', response.data.data, {
                        root: true
                    })
                })
                .catch(err => {
                    localStorage.clear();
                    // this.$router.push({path:'/login'})
                    location.reload();
                    // vm.$forceUpdate();
                })
        })
    },

    register({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/register`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
