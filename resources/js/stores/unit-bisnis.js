import $axios from '../api.js'

const state = () => ({
    unitBisnies: [],
    ddUnitBisnies: [],
    page: 1,
    id: ''
})

const mutations = {
    ASSIGN_DATA(state, payload) {
        state.unitBisnies = payload
    },
    ASSIGN_DROPDOWN(state, payload) {
        state.ddUnitBisnies = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ID_UPDATE(state, payload) {
        state.id = payload
    }
}

const actions = {
    getUnitBisnies({
        commit,
        state
    }, payload) {
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/unit-bisnis?search=${search.keyword}&page=${search.page}&limit=${search.limit}`)
                .then((response) => {
                    commit('ASSIGN_DATA', response.data.data)
                    resolve(response.data.meta)
                })
        })
    },

    ddUnitBisnies({
        commit,
        state
    }) {
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.get(`/unit-bisnis-dropdown`)
                .then((response) => {
                    commit('ASSIGN_DROPDOWN', response.data.data)
                    resolve(response.data.data)
                })
        })
    },

    addUnitBisnies({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.post(`/unit-bisnis/add`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    updateUnitBisnies({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.put(`/unit-bisnis/${state.id}`, payload)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },

    deleteUnitBisnies({
        commit,
        state
    }, payload) {
        // let search = typeof payload != 'undefined' ? payload:''
        // console.log(payload)
        return new Promise((resolve, reject) => {
            // $axios.get(`/pendidikan?page=${state.page}&q=${search}`)
            $axios.delete(`/unit-bisnis/${payload}`)
                .then((response) => {
                    resolve(response.data.data)
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
