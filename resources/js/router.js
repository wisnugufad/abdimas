//IMPORT SECTION
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Index from './components/Index.vue'
import store from './store.js'
import Master from './router/master'

Vue.use(Router)

//DEFINE ROUTE
const router = new Router({
    mode: 'history',
    routes: [{
            path: '',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/master',
            name: 'master',
            component: Index,
            meta: {
                requiresAuth: true
            },
            children: Master
        },
        {
            path: '/dosen',
            component: Index,
            meta: {
                requiresAuth: true
            },
            children: [{
                    path: '',
                    name: 'dosen',
                    component: () => import(`./views/dosen/Dosen`)
                },
                {
                    path: 'profil/:id',
                    name: 'dosen.profil',
                    component: () => import(`./views/dosen/Profile`)
                },
            ]

        },
        {
            path: '/mahasiswa',
            name: 'mahasiswa',
            component: () => import(`./views/mahasiswa/Mahasiswa`)
        },
        {
            path: '/pengajuan',
            component: Index,
            meta: {
                requiresAuth: true
            },
            children: [{
                    path: '',
                    name: 'pengajuan',
                    component: () => import(`./views/pengajuan-abdimas/PengajuanAbdimas`),
                },
                {
                    path: 'view/:id',
                    name: 'view_pengajuan',
                    component: () => import(`./views/pengajuan-abdimas/ViewPengajuan`),
                }
            ]
        },
        {
            path: '/approval',
            component: Index,
            meta: {
                requiresAuth: true
            },
            children: [{
                    path: '',
                    name: 'submited',
                    component: () => import(`./views/approval/Submited`)
                },
                {
                    path: 'approve',
                    name: 'approve',
                    component: () => import(`./views/approval/Approve`)
                },
                {
                    path: 'rejected',
                    name: 'rejected',
                    component: () => import(`./views/approval/Rejected`)
                },
            ]

        },
        {
            path: '/surat',
            component: Index,
            meta: {
                requiresAuth: true
            },
            children: [{
                    path: 'surat-tugas',
                    name: 'SuratTugas',
                    component: () => import(`./views/surat/SuratTugas`)
                },
                {
                    path: 'lembar-pengesahan',
                    name: 'LembarPengesahan',
                    component: () => import(`./views/surat/LembarPengesahan`)
                }
            ]

        },
        {
            path: '/pengurus',
            name: 'pengurus',
            component: () => import(`./views/pengurus/Pengurus`)
        },
        {
            path: '/print',
            name: 'print',
            component: () => import(`./views/print/SuratPelaporan`)
        },
        {
            path: '/404',
            name: 'error',
            component: () => import(`./components/NotFound`)
        },
    ]
});

//Navigation Guards
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let auth = store.getters.isAuth
        if (!auth) {
            next({
                name: 'login'
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router
