import Vue from 'vue'
import router from './router.js'
import store from './store.js'
import App from './App.vue'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import Pagination from './components/Pagination'
import {
    Chart
} from "frappe-charts"
// import {
//     Chart
// } from 'frappe-charts/dist/frappe-charts.esm.js'
// import css
import 'frappe-charts/dist/frappe-charts.min.css'

Vue.use(Element, {
    locale,
    Chart
})
Vue.component('pagination', Pagination)

new Vue({
    el: '#dw',
    router,
    store,
    components: {
        App
    }
})
