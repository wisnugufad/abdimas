import Vue from 'vue'
import Vuex from 'vuex'

//IMPORT MODULE SECTION
import auth from './stores/auth.js'
import pendidikan from './stores/pendidikan.js'
import dana from './stores/dana.js'
import kepangkatan from './stores/kepangkatan.js'
import publikasi from './stores/publikasi.js'
import mitra from './stores/mitra'
import iptek from './stores/iptek'
import dosen from './stores/dosen'
import mahasiswa from './stores/mahasiswa'
import prodi from './stores/prodi'
import mitraOutput from './stores/mitra-output'
import typical from './stores/typical'
import unitBisnis from './stores/unit-bisnis'
import pengajuan from './stores/pengajuan'
import approve from './stores/approve'
import dashboard from './stores/dashboard'
import semester from './stores/semester'
import surat from './stores/surat'
import pengurus from './stores/pengurus'

Vue.use(Vuex)

//DEFINE ROOT STORE VUEX
const store = new Vuex.Store({
    //SEMUA MODULE YANG DIBUAT AKAN DITEPATKAN DIDALAM BAGIAN INI DAN DIPISAHKAN DENGAN KOMA UNTUK SETIAP MODULE-NYA
    modules: {
        auth,
        pendidikan,
        kepangkatan,
        dana,
        publikasi,
        mitra,
        mitraOutput,
        iptek,
        unitBisnis,
        typical,
        dosen,
        mahasiswa,
        prodi,
        pengajuan,
        approve,
        dashboard,
        semester,
        surat,
        pengurus
    },
    //STATE HAMPIR SERUPA DENGAN PROPERTY DATA DARI COMPONENT HANYA SAJA DAPAT DIGUNAKAN SECARA GLOBAL
    state: {
        //VARIABLE TOKEN MENGAMBIL VALUE DARI LOCAL STORAGE token
        token: localStorage.getItem('token'),
        user: [],
        errors: []
    },
    getters: {
        //KITA MEMBUAT SEBUAH GETTERS DENGAN NAMA isAuth
        //DIMANA GETTERS INI AKAN BERNILAI TRUE/FALSE DENGAN KONDISI BERDASARKAN
        //STATE token.
        isAuth: state => {
            return state.token !== "null" &&
                state.token !== null
        },
        isUser: state => {
            return state.user
        }
    },
    mutations: {
        //SEBUAH MUTATIONS YANG BERFUNGSI UNTUK MEMANIPULASI VALUE DARI STATE token
        SET_TOKEN(state, payload) {
            state.token = payload
        },
        SET_USER(state, payload) {
            state.user = payload
        },
        SET_ERRORS(state, payload) {
            state.errors = payload
        },
        CLEAR_ERRORS(state) {
            state.errors = []
        }
    }
})

export default store
