const master = [{
        path: '/prodi',
        name: 'prodi',
        component: () => import(`../views/prodi/Prodi`)
    },
    {
        path: '/semester',
        name: 'semester',
        component: () => import(`../views/semester/Semester`)
    },
    {
        path: '/pendidikan',
        name: 'pendidikan',
        component: () => import(`../views/pendidikan/Pendidikan`)
    },
    {
        path: '/kepangkatan',
        name: 'kepangkatan',
        component: () => import(`../views/kepangkatan/Kepangkatan`)
    },
    {
        path: '/dana',
        name: 'dana',
        component: () => import(`../views/dana/Dana`)
    },
    {
        path: '/publikasi',
        name: 'publikasi',
        component: () => import(`../views/publikasi/Publikasi`)
    },
    {
        path: '/mitra',
        name: 'mitra',
        component: () => import(`../views/mitra/Mitra`)
    },
    {
        path: '/mitra-output',
        name: 'mitraOutput',
        component: () => import(`../views/mitra-output/MitraOutput`)
    },
    {
        path: '/iptek',
        name: 'iptek',
        component: () => import(`../views/iptek/Iptek`)
    },
    {
        path: '/unit-bisnis',
        name: 'unitBisnis',
        component: () => import(`../views/unit-bisnis/UnitBisnis`)
    },
    {
        path: '/typical',
        name: 'typical',
        component: () => import(`../views/typical/Typical`)
    },
]

export default master;
