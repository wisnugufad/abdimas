(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! html2canvas */ "./node_modules/html2canvas/dist/html2canvas.js");
/* harmony import */ var html2canvas__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "HelloWorld",
  props: {
    msg: String
  },
  data: function data() {
    return {
      tanggal: this.getDate(),
      comboProdi: [],
      pengurus: [],
      loading: false,
      generate: false,
      prodi: {
        nama_prodi: "",
        nidn: "",
        name: "",
        gelar: ""
      },
      search: {
        prodi: "",
        kode: ""
      },
      dataAbdimas: [],
      ketua: {
        name: "Ir. Sumiarti, M.Kom",
        nidn: "0310096101",
        pangkat: "Lektor Kepala",
        jurusan: "Informatika  (IF)",
        hp: "08129234100",
        email: "sumiarti.andri@iti.ac.id"
      },
      anggota: [{
        name: "Ir. Sumiarti, M.Kom",
        nidn: "0310096101",
        pangkat: "Lektor Kepala",
        jurusan: "Informatika  (IF)",
        hp: "08129234100",
        email: "sumiarti.andri@iti.ac.id"
      }]
    };
  },
  created: function created() {
    var _this = this;

    this.ddProdis().then(function () {
      _this.comboProdi = _this.$store.state.prodi.ddProdis;
    });
    this.getPengurus().then(function () {
      _this.pengurus = _this.$store.state.pengurus.pengurus;
    });
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapState"])("surat", {
    surat: function surat(state) {
      return state.surats;
    }
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("prodi", ["ddProdis"]), {}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("pengurus", ["getPengurus"]), {}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("surat", ["getPengesahan"]), {
    generateSurat: function generateSurat() {
      var _this2 = this;

      this.loading = true;
      this.generate = false;
      this.getProdiDosen();
      this.getPengesahan(this.search).then(function () {
        _this2.loading = false;

        _this2.$nextTick(function () {
          _this2.generate = true;
        });
      });
    },
    formatRupiah: function formatRupiah(angka) {
      var intAngka = parseInt(angka);
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
          split = number_string.split(","),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi); // tambahkan titik jika yang di input sudah menjadi angka ribuan

      if (ribuan) {
        var separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return "Rp. " + rupiah + ",-";
    },
    getProdiDosen: function getProdiDosen() {
      var _this3 = this;

      this.prodi = this.comboProdi.find(function (x) {
        return x.id === _this3.search.prodi;
      });
    },
    getDate: function getDate() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var bln = "";
      var yyyy = today.getFullYear();

      if (dd < 10) {
        dd = "0" + dd;
      }

      if (mm < 10) {
        mm = "0" + mm;
      }

      switch (mm) {
        case "01":
          bln = "Januari";
          break;

        case "02":
          bln = "Febuari";
          break;

        case "03":
          bln = "Maret";
          break;

        case "04":
          bln = "April";
          break;

        case "05":
          bln = "Mei";
          break;

        case "06":
          bln = "Juni";
          break;

        case "07":
          bln = "Juli";
          break;

        case "08":
          bln = "Agustus";
          break;

        case "09":
          bln = "September";
          break;

        case "10":
          bln = "Oktober";
          break;

        case "11":
          bln = "November";
          break;

        default:
          bln = "Desember";
          break;
      } //   return yyyy + "-" + mm + "-" + dd;


      return dd + " " + bln + " " + yyyy;
    },
    download: function download() {
      window.print();
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.surat[data-v-50bdb37a] {\n  padding: 20px;\n  max-width: 794px;\n  margin: auto !important;\n}\n.surat td[data-v-50bdb37a] {\n  vertical-align: top;\n}\n/* .surat h2,\nh3 {\n\n} */\n@media print {\nbody *[data-v-50bdb37a] {\n    visibility: hidden;\n}\n.main-footer[data-v-50bdb37a] {\n    visibility: hidden;\n}\nfooter *[data-v-50bdb37a] {\n    visibility: hidden;\n}\n.surat[data-v-50bdb37a] {\n    max-width: none;\n}\n#anggota[data-v-50bdb37a] {\n    margin-left: -128px !important;\n}\n#headerSurat[data-v-50bdb37a],\n  #headerSurat *[data-v-50bdb37a] {\n    display: none;\n}\n#kopsurat[data-v-50bdb37a],\n  #kopsurat *[data-v-50bdb37a] {\n    visibility: visible;\n}\n#kopsurat[data-v-50bdb37a] {\n    /* position: absolute; */\n    left: 0;\n    top: 0;\n    margin-top: -50px;\n}\n}\n.persetujuan td[data-v-50bdb37a] {\n  text-align: center;\n}\n.isiSurat[data-v-50bdb37a] {\n  margin-left: 100px;\n  margin-right: 50px;\n}\n.isiSurat td[data-v-50bdb37a] {\n  padding: 2px 5px 2px 5px;\n  /* font-size: 12px; */\n}\n@page {\n  size: auto; /* auto is the initial value */\n  margin: 50px 0px 100px 0px; /* this affects the margin in the printer settings */\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "content" }, [
    _c("div", { staticClass: "box" }, [
      _c(
        "div",
        { staticClass: "box-header with-border", attrs: { id: "headerSurat" } },
        [
          _c("br"),
          _vm._v(" "),
          _c(
            "el-row",
            [
              _c(
                "el-col",
                { attrs: { span: 12 } },
                [
                  _c(
                    "el-form",
                    {
                      ref: "dataForm",
                      staticStyle: { width: "80%" },
                      attrs: { "label-position": "top", "label-width": "100px" }
                    },
                    [
                      _c(
                        "el-form-item",
                        {
                          attrs: {
                            label: "Prodi Jurusan",
                            "label-position": "top"
                          }
                        },
                        [
                          _c(
                            "el-select",
                            {
                              attrs: { placeholder: "Please select" },
                              model: {
                                value: _vm.search.prodi,
                                callback: function($$v) {
                                  _vm.$set(_vm.search, "prodi", $$v)
                                },
                                expression: "search.prodi"
                              }
                            },
                            _vm._l(_vm.comboProdi, function(item) {
                              return _c("el-option", {
                                key: item.id,
                                attrs: {
                                  label:
                                    "[" +
                                    item.kode_prodi +
                                    "] " +
                                    item.nama_prodi,
                                  value: item.id
                                }
                              })
                            }),
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-form-item",
                        {
                          attrs: {
                            label: "Kode Pengabdian",
                            "label-position": "top"
                          }
                        },
                        [
                          _c("el-input", {
                            attrs: { placeholder: "IF/2020/1/NIDN/12345678/1" },
                            model: {
                              value: _vm.search.kode,
                              callback: function($$v) {
                                _vm.$set(_vm.search, "kode", $$v)
                              },
                              expression: "search.kode"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: {
                        type: "primary",
                        icon: "el-icon-s-claim",
                        disabled:
                          this.search.prodi === "" || this.search.kode === ""
                      },
                      on: {
                        click: function($event) {
                          return _vm.generateSurat()
                        }
                      }
                    },
                    [_vm._v("Buat Pengesahan")]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: {
                        size: "small",
                        type: "success",
                        icon: "el-icon-edit"
                      },
                      on: {
                        click: function($event) {
                          return _vm.download()
                        }
                      }
                    },
                    [_vm._v("Print")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 8 } })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.generate
        ? _c(
            "div",
            { staticClass: "box-body", attrs: { loading: _vm.loading } },
            [
              _c("div", { staticClass: "surat", attrs: { align: "center" } }, [
                _c(
                  "div",
                  { ref: "kopsurat", attrs: { id: "kopsurat" } },
                  [
                    _c("table", { attrs: { width: "95%" } }, [
                      _c("tr", [
                        _c("td", { attrs: { width: "10%" } }, [
                          _c("img", {
                            attrs: {
                              src: "/img/iti-logo.png",
                              alt: "image",
                              width: "100px"
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("td", { attrs: { width: "10px" } }),
                        _vm._v(" "),
                        _vm._m(0),
                        _vm._v(" "),
                        _c("td", { attrs: { width: "10%" } }),
                        _vm._v(" "),
                        _vm._m(1)
                      ])
                    ]),
                    _vm._v(" "),
                    _c("hr", {
                      staticStyle: { "border-top": "2px solid black" }
                    }),
                    _vm._v(" "),
                    _c(
                      "h3",
                      {
                        staticStyle: { "font-weight": "bold" },
                        attrs: { align: "center" }
                      },
                      [_vm._v("HALAMAN PENGESAHAN")]
                    ),
                    _vm._v(" "),
                    _c("table", { staticClass: "isiSurat" }, [
                      _c("tr", [
                        _vm._m(2),
                        _vm._v(" "),
                        _c("td", [_vm._v(":")]),
                        _vm._v(" "),
                        _c("td", [
                          _c(
                            "span",
                            { staticStyle: { "font-weight": "bold" } },
                            [_vm._v(_vm._s(_vm.surat.judul))]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _vm._m(3),
                        _vm._v(" "),
                        _c("td", [_vm._v(":")]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.bidang))])
                      ]),
                      _vm._v(" "),
                      _vm._m(4),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("a.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("Nama Lengkap")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.name))])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("b.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("NIDN")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.nidn))])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("c.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("Jabatan Fungsional")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.pangkat))])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("d.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("Program Studi")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.nama_prodi))])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("e.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("Nomor HP")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.telp))])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", [_vm._v("f.")]),
                        _vm._v(" "),
                        _c("td", [_vm._v("Email")]),
                        _vm._v(" "),
                        _c("td"),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(_vm.surat.email))])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _vm._l(_vm.surat.anggota, function(item, index) {
                      return _c(
                        "table",
                        {
                          key: index,
                          staticClass: "isiSurat",
                          staticStyle: { "margin-left": "-180px" },
                          attrs: { id: "anggota" }
                        },
                        [
                          item.role === 2
                            ? _c("div", [
                                _c("tr", [
                                  _c("td", [_vm._v("a.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Nama Lengkap")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.name))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("b.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("NIDN")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.nidn))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("c.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Jabatan Fungsional")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.pangkat))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("d.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Program Studi")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.prodi))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("e.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Nomor HP")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.telp))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("f.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Email")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.email))])
                                ]),
                                _vm._v(" "),
                                _vm._m(5, true)
                              ])
                            : _c("div", [
                                _c("tr", [
                                  _c("td", [_vm._v("a.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Nama Lengkap")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.name))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("b.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("NRP")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.nrp))])
                                ]),
                                _vm._v(" "),
                                _vm._m(6, true),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("d.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Program Studi")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.prodi))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("e.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Nomor HP")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.telp))])
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c("td", [_vm._v("f.")]),
                                  _vm._v(" "),
                                  _c("td", [_vm._v("Email")]),
                                  _vm._v(" "),
                                  _c("td"),
                                  _vm._v(" "),
                                  _c("td", [_vm._v(_vm._s(item.email))])
                                ]),
                                _vm._v(" "),
                                _vm._m(7, true)
                              ])
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c(
                      "table",
                      { staticClass: "isiSurat", attrs: { width: "80%" } },
                      [
                        _vm._m(8),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", { attrs: { width: "150px" } }, [
                            _vm._v("Nama Institusi")
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(":")]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(_vm.surat.mitra[0].name))])
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", [_vm._v("Alamat")]),
                          _vm._v(" "),
                          _c("td", [_vm._v(":")]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(_vm.surat.mitra[0].alamat))])
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", [_vm._v("Tahun Pelaksanaan")]),
                          _vm._v(" "),
                          _c("td", [_vm._v(":")]),
                          _vm._v(" "),
                          _c("td", [_vm._v("Tahun " + _vm._s(_vm.surat.tahun))])
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", [_vm._v("Biaya keseluruhan")]),
                          _vm._v(" "),
                          _c("td", [_vm._v(":")]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                this.formatRupiah(_vm.surat.dana[0].jumlah)
                              )
                            )
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("div", [
                      _c(
                        "span",
                        {
                          staticStyle: {
                            float: "right",
                            "padding-right": "100px"
                          }
                        },
                        [_vm._v("Tangerang Selatan, " + _vm._s(this.tanggal))]
                      ),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c(
                        "table",
                        {
                          staticClass: "persetujuan",
                          attrs: { widht: "100%", align: "center" }
                        },
                        [
                          _c("tr", [
                            _c("td", { attrs: { width: "35%" } }, [
                              _c("p", [_vm._v("Mengetahui,")]),
                              _vm._v(
                                "\n                  Ketua Prodi " +
                                  _vm._s(this.prodi.nama_prodi) +
                                  "\n                "
                              )
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { "min-width": "30%" } }),
                            _vm._v(" "),
                            _c("td", { attrs: { width: "35%" } }, [
                              _vm._v("Pelaksana,")
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(9),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td", [
                              _vm._v(
                                "( " +
                                  _vm._s(this.prodi.name) +
                                  ", " +
                                  _vm._s(this.prodi.gelar) +
                                  " )"
                              )
                            ]),
                            _vm._v(" "),
                            _c("td"),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v("( " + _vm._s(_vm.surat.name) + " )")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td", [
                              _vm._v("NIDN: " + _vm._s(this.prodi.nidn))
                            ]),
                            _vm._v(" "),
                            _c("td"),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v("NIDN: " + _vm._s(_vm.surat.nidn))
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(10),
                          _vm._v(" "),
                          _vm._m(11),
                          _vm._v(" "),
                          _vm._m(12),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td", { attrs: { colspan: "3" } }, [
                              _c("p", [_vm._v(_vm._s(this.pengurus.name))]),
                              _vm._v(" "),
                              _c("p", [
                                _vm._v("NIDN : " + _vm._s(this.pengurus.nidn))
                              ])
                            ])
                          ])
                        ]
                      )
                    ])
                  ],
                  2
                )
              ])
            ]
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c(
        "h3",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("INSTITUT")]
      ),
      _vm._v(" "),
      _c(
        "h3",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("TEKNOLOGI")]
      ),
      _vm._v(" "),
      _c(
        "h3",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("INDONESIA")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", { attrs: { align: "center", width: "50%" } }, [
      _c(
        "h4",
        {
          staticStyle: {
            "font-weight": "bold",
            "margin-top": "0px",
            "margin-bottom": "0px"
          }
        },
        [_vm._v("KAMPUS")]
      ),
      _vm._v(" "),
      _c(
        "h5",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("Jl. Raya Puspiptek Serpong")]
      ),
      _vm._v(" "),
      _c(
        "h5",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("Tangerang - Selatan 15320")]
      ),
      _vm._v(" "),
      _c(
        "h5",
        { staticStyle: { "margin-top": "0px", "margin-bottom": "0px" } },
        [_vm._v("(021) 7560542 - 7560545 Fax. (021) 7560542")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", { attrs: { colspan: "2" } }, [
      _c("span", { staticStyle: { "font-weight": "bold" } }, [_vm._v("Judul")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", { attrs: { colspan: "2" } }, [
      _c("span", { staticStyle: { "font-weight": "bold" } }, [
        _vm._v("Kelompok P2M")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { attrs: { colspan: "4" } }, [
        _c("span", { staticStyle: { "font-weight": "bold" } }, [
          _vm._v("Pelaksana")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { colspan: "4" } }, [_c("br")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [_vm._v("c.")]),
      _vm._v(" "),
      _c("td", [_vm._v("Jabatan Fungsional")]),
      _vm._v(" "),
      _c("td"),
      _vm._v(" "),
      _c("td", [_vm._v("Mahasiswa")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { colspan: "4" } }, [_c("br")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { attrs: { colspan: "3" } }, [_vm._v("Institusi Mitra")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { height: "70px", colspan: "3" } })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { height: "50px", colspan: "3" } })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { attrs: { colspan: "3" } }, [
        _c("p", [_vm._v("Menyetujui,")]),
        _vm._v(" "),
        _c("p", [
          _vm._v("Direktur Lembaga Pengabdian dan Pemberdayaan Masyarakat")
        ]),
        _vm._v(" "),
        _c("p", [_vm._v("( LP2M )- ITI")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { height: "70px", colspan: "3" } })])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/surat/LembarPengesahan.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/surat/LembarPengesahan.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true& */ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true&");
/* harmony import */ var _LembarPengesahan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LembarPengesahan.vue?vue&type=script&lang=js& */ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& */ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _LembarPengesahan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "50bdb37a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/surat/LembarPengesahan.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./LembarPengesahan.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=style&index=0&id=50bdb37a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_style_index_0_id_50bdb37a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/LembarPengesahan.vue?vue&type=template&id=50bdb37a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LembarPengesahan_vue_vue_type_template_id_50bdb37a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);