(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return _defineProperty({
      tanggal: this.getDate(),
      search: "",
      loading: false,
      generate: false,
      dataAbdimas: [],
      comboProdi: [],
      comboSemester: [],
      pengurus: [],
      no_surat: "",
      prodi: [],
      semester: []
    }, "search", {
      prodi: "",
      semester: ""
    });
  },
  created: function created() {
    var _this = this;

    this.ddProdis().then(function () {
      _this.comboProdi = _this.$store.state.prodi.ddProdis;
    });
    this.getPengurus().then(function () {
      _this.pengurus = _this.$store.state.pengurus.pengurus;
    });
    this.ddSemesters().then(function () {
      _this.comboSemester = _this.$store.state.semester.ddSemesters;
    });
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapState"])("surat", {
    surat: function surat(state) {
      return state.surats;
    }
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("prodi", ["ddProdis"]), {}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("pengurus", ["getPengurus"]), {}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("semester", ["ddSemesters"]), {}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])("surat", ["getSuratTugas"]), {
    generateSurat: function generateSurat() {
      var _this2 = this;

      this.loading = true;
      this.generate = false;
      this.findSemester();
      this.getProdiDosen();
      this.getSuratTugas(this.search).then(function () {
        _this2.dataAbdimas = _this2.$store.state.surat.surats;
        _this2.loading = false;

        _this2.$nextTick(function () {
          _this2.generate = true;
        });
      });
    },
    findSemester: function findSemester() {
      var _this3 = this;

      this.semester = this.comboSemester.find(function (x) {
        return x.id === _this3.search.semester;
      });
    },
    getProdiDosen: function getProdiDosen() {
      var _this4 = this;

      this.prodi = this.comboProdi.find(function (x) {
        return x.id === _this4.search.prodi;
      });
    },
    getDate: function getDate() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();

      if (dd < 10) {
        dd = "0" + dd;
      }

      if (mm < 10) {
        mm = "0" + mm;
      }

      var bln = "";

      switch (mm) {
        case "01":
          bln = "Januari";
          break;

        case "02":
          bln = "Febuari";
          break;

        case "03":
          bln = "Maret";
          break;

        case "04":
          bln = "April";
          break;

        case "05":
          bln = "Mei";
          break;

        case "06":
          bln = "Juni";
          break;

        case "07":
          bln = "Juli";
          break;

        case "08":
          bln = "Agustus";
          break;

        case "09":
          bln = "September";
          break;

        case "10":
          bln = "Oktober";
          break;

        case "11":
          bln = "November";
          break;

        default:
          bln = "Desember";
          break;
      } //   return yyyy + "-" + mm + "-" + dd;


      return dd + " " + bln + " " + yyyy;
    },
    download: function download() {
      var img = new Image();
      img.src = __webpack_require__(/*! ../../../../public/img/kop-surat.jpg */ "./public/img/kop-surat.jpg");
      var doc = new jspdf__WEBPACK_IMPORTED_MODULE_1___default.a("p", "pt", "a4");
      var width = doc.internal.pageSize.getWidth();
      var height = doc.internal.pageSize.getHeight();
      doc.addImage(img, "jpeg", 0, 0, width, 100);
      doc.setFontSize(14);
      doc.text(width / 2, 120, "SURAT TUGAS", {
        align: "center"
      });
      var noSurat = "No : " + this.no_surat;
      doc.setFontSize(12);
      doc.text(width / 2, 140, noSurat, {
        align: "center"
      });
      var elem = document.getElementById("testTable");
      var persetujuanTTD = document.getElementById("persetujuan");
      doc.autoTable({
        html: elem,
        theme: "plain",
        useCss: true,
        margin: {
          top: 150,
          horizontal: 80,
          bottom: 60
        },
        styles: {
          cellPadding: 3,
          minCellHeight: 10,
          valign: "center",
          fontSize: 12,
          textColor: 0
        },
        columnStyles: {
          0: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.17
          },
          1: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.03
          },
          2: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.6
          }
        }
      });
      doc.setFontSize(10);
      doc.text(80, 650, "Tembusan Yth.");
      doc.text(80, 660, "1.   Wakil Rektor Bid. Sumberdaya, Bisnis dan Kerjasama");
      doc.text(80, 670, "2.   Ka. Divisi SDM");
      doc.text(80, 680, "3.   Prodi " + this.prodi.nama_prodi);
      doc.text(80, 690, "4.   Arsip");
      doc.autoTable({
        html: persetujuanTTD,
        theme: "plain",
        useCss: true,
        margin: {
          top: 300,
          horizontal: 40,
          bottom: 60
        },
        styles: {
          cellPadding: 3,
          minCellHeight: 10,
          valign: "top",
          fontSize: 12,
          textColor: 0
        },
        columnStyles: {
          0: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.2
          },
          1: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.35
          },
          2: {
            cellWidth: doc.internal.pageSize.getWidth() * 0.3
          }
        }
      });
      doc.addPage("a4", "lanscape"); //   var abdimas = document.getElementById("abdimas");
      // head: [['Name', 'Email', 'Country']],
      // body: [
      //     ['David', 'david@example.com', 'Sweden'],
      //     ['Castille', 'castille@example.com', 'Norway'],
      //     // ...
      // ]

      var widthLanscape = doc.internal.pageSize.getWidth(); //   var height = doc.internal.pageSize.getHeight();

      doc.text(widthLanscape / 2, 30, "Lampiran Surat Tugas", {
        align: "center"
      });
      doc.text(widthLanscape / 2, 45, noSurat, {
        align: "center"
      });
      doc.text(widthLanscape / 2, 60, this.tanggal, {
        align: "center"
      });
      var keterangan = "DAFTAR ABDIMAS DOSEN PROGRAM STUDI " + this.prodi.nama_prodi + "" + this.semester.semester;
      doc.text(widthLanscape / 2, 75, keterangan, {
        align: "center"
      });
      var bodyTable = [];
      this.dataAbdimas.map(function (item, index) {
        return bodyTable.push({
          no: index + 1,
          judul: item.judul,
          bidang: item.bidang,
          name: item.name,
          sumber_dana: item.dana,
          jabatan: item.jabatan,
          keterlibatan: item.keterlibatan,
          dana: item.jumlah,
          SKA: item.SKA,
          mahasiswa: item.mahasiswa
        });
      });
      doc.autoTable({
        // html: abdimas,
        theme: "striped",
        // useCss: true,
        margin: {
          top: 80,
          horizontal: 40,
          bottom: 60
        },
        // head: [headTable],
        columns: [{
          header: "No",
          dataKey: "no"
        }, {
          header: "TOPIK ABDIMAS",
          dataKey: "judul"
        }, {
          header: "BIDANG",
          dataKey: "bidang"
        }, {
          header: "NAMA DOSEN",
          dataKey: "name"
        }, {
          header: "SUMBER DANA",
          dataKey: "sumber_dana"
        }, {
          header: "JABATAN (KETUA/ANGGOTA)",
          dataKey: "jabatan"
        }, {
          header: "KETERLIBATAN PRODI/INSTITUSI LAIN",
          dataKey: "keterlibatan"
        }, {
          header: "DANA",
          dataKey: "dana"
        }, {
          header: "SKA",
          dataKey: "SKA"
        }, {
          header: "KETERLIBATAN MAHASISWA",
          dataKey: "mahasiswa"
        }],
        body: bodyTable,
        pageBreak: "auto",
        // 'auto', 'avoid' or 'always'
        tableWidth: "auto",
        styles: {
          cellPadding: 5,
          minCellWidth: 20,
          minCellHeight: 10,
          valign: "top",
          fontSize: 8 //   textColor: 0

        },
        columnStyles: {
          0: {
            cellWidth: 20
          },
          1: {
            cellWidth: 250
          },
          2: {
            cellWidth: "wrap"
          },
          3: {
            cellWidth: "wrap"
          },
          4: {
            cellWidth: "wrap"
          },
          5: {
            cellWidth: "wrap"
          },
          6: {
            cellWidth: "wrap"
          },
          7: {
            cellWidth: "wrap"
          },
          8: {
            cellWidth: 40
          },
          9: {
            cellWidth: "wrap"
          }
        }
      });
      doc.autoTable({
        html: persetujuanTTD,
        theme: "plain",
        useCss: false,
        margin: {
          top: 200,
          horizontal: 40,
          bottom: 10
        },
        styles: {
          cellPadding: 3,
          minCellHeight: 10,
          halign: "center",
          valign: "top",
          fontSize: 6,
          textColor: 0
        },
        columnStyles: {
          0: {
            cellWidth: widthLanscape * 0.2
          },
          1: {
            cellWidth: widthLanscape * 0.45
          },
          2: {
            cellWidth: widthLanscape * 0.2
          }
        }
      });
      doc.save("Surat Tugas " + this.no_surat + ".pdf"); //   window.print();
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.surat[data-v-d8b2e0c0] {\n  padding: 20px;\n}\n/* .surat h2,\nh3 {\n\n} */\n@media print {\nbody *[data-v-d8b2e0c0] {\n    visibility: hidden;\n}\n.main-footer[data-v-d8b2e0c0] {\n    visibility: hidden;\n}\nfooter *[data-v-d8b2e0c0] {\n    visibility: hidden;\n}\n#headerSurat[data-v-d8b2e0c0],\n  #headerSurat *[data-v-d8b2e0c0] {\n    display: none;\n}\n.abdimas[data-v-d8b2e0c0] {\n    /* display: block; */\n    /* transform: rotate(270deg); */\n    /* position: absolute; */\n    font-size: 10px;\n    margin: 10 10 10 10;\n}\n  /* @page abdimas {\n    size: landscape;\n  } */\n#kopsurat[data-v-d8b2e0c0],\n  #kopsurat *[data-v-d8b2e0c0] {\n    visibility: visible;\n}\n#kopsurat[data-v-d8b2e0c0] {\n    /* position: absolute; */\n    left: 0;\n    top: 0;\n    margin-top: -50px;\n}\n\n  /* div.portrait,\n  div.landscape {\n    margin: 0;\n    padding: 0;\n    border: none;\n    background: none;\n  }\n  div.landscape {\n    transform: rotate(270deg) translate(-276mm, 0);\n    transform-origin: 0 0;\n  } */\n}\n.abdimas th[data-v-d8b2e0c0] {\n  text-align: center;\n}\n.abdimas td[data-v-d8b2e0c0] {\n  padding: 2px 5px 2px 5px;\n}\n.persetujuan p[data-v-d8b2e0c0] {\n  text-align: center;\n  margin: 0 0 0 0;\n}\n.tembusan[data-v-d8b2e0c0] {\n  margin-top: 170px;\n  margin-left: 50px;\n  margin-right: 50px;\n}\n.tembusan ol[data-v-d8b2e0c0] {\n  padding-left: 13px;\n}\n.tembusan li[data-v-d8b2e0c0] {\n  padding-left: 15px;\n}\n.isiSurat[data-v-d8b2e0c0] {\n  margin-left: 50px;\n  margin-right: 50px;\n}\n.isiSurat td[data-v-d8b2e0c0] {\n  padding: 2px 5px 2px 5px;\n  /* font-size: 12px; */\n}\n@page {\n  size: auto; /* auto is the initial value */\n  margin: 50px 0px 100px 0px; /* this affects the margin in the printer settings */\n}\n\n/* page break */\n.pagebreak[data-v-d8b2e0c0] {\n  page-break-before: always;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "content" }, [
    _c("div", { staticClass: "box" }, [
      _c(
        "div",
        { staticClass: "box-header with-border", attrs: { id: "headerSurat" } },
        [
          _c("br"),
          _vm._v(" "),
          _c(
            "el-row",
            [
              _c(
                "el-col",
                { attrs: { span: 12 } },
                [
                  _c(
                    "el-form",
                    {
                      ref: "dataForm",
                      staticStyle: { width: "80%" },
                      attrs: { "label-position": "top", "label-width": "100px" }
                    },
                    [
                      _c(
                        "el-form-item",
                        {
                          attrs: {
                            label: "Prodi Jurusan",
                            "label-position": "top"
                          }
                        },
                        [
                          _c(
                            "el-select",
                            {
                              attrs: { placeholder: "Please select" },
                              model: {
                                value: _vm.search.prodi,
                                callback: function($$v) {
                                  _vm.$set(_vm.search, "prodi", $$v)
                                },
                                expression: "search.prodi"
                              }
                            },
                            _vm._l(_vm.comboProdi, function(item) {
                              return _c("el-option", {
                                key: item.id,
                                attrs: {
                                  label:
                                    "[" +
                                    item.kode_prodi +
                                    "] " +
                                    item.nama_prodi,
                                  value: item.id
                                }
                              })
                            }),
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form",
                    {
                      ref: "dataForm",
                      staticStyle: { width: "80%" },
                      attrs: { "label-position": "top", "label-width": "100px" }
                    },
                    [
                      _c(
                        "el-form-item",
                        {
                          attrs: { label: "Semester", "label-position": "top" }
                        },
                        [
                          _c(
                            "el-select",
                            {
                              attrs: { placeholder: "Please select" },
                              model: {
                                value: _vm.search.semester,
                                callback: function($$v) {
                                  _vm.$set(_vm.search, "semester", $$v)
                                },
                                expression: "search.semester"
                              }
                            },
                            _vm._l(_vm.comboSemester, function(item) {
                              return _c("el-option", {
                                key: item.id,
                                attrs: { label: item.semester, value: item.id }
                              })
                            }),
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "el-form-item",
                        { attrs: { label: "No Surat Tugas" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "text",
                              placeholder: "032/ST-ABD/LP2M-ITI/VIII/2019"
                            },
                            model: {
                              value: _vm.no_surat,
                              callback: function($$v) {
                                _vm.no_surat = $$v
                              },
                              expression: "no_surat"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: { type: "primary", icon: "el-icon-edit" },
                      on: {
                        click: function($event) {
                          return _vm.generateSurat()
                        }
                      }
                    },
                    [_vm._v("generate")]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: {
                        size: "small",
                        type: "primary",
                        icon: "el-icon-edit"
                      },
                      on: {
                        click: function($event) {
                          return _vm.download()
                        }
                      }
                    },
                    [_vm._v("Print")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 12 } })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "box-body" }, [
        _vm.generate
          ? _c(
              "div",
              {
                directives: [
                  {
                    name: "loading",
                    rawName: "v-loading",
                    value: _vm.loading,
                    expression: "loading"
                  }
                ],
                staticClass: "surat"
              },
              [
                _c(
                  "div",
                  {
                    ref: "kopsurat",
                    staticClass: "portrait",
                    attrs: { id: "kopsurat" }
                  },
                  [
                    _c(
                      "h3",
                      {
                        ref: "surattugas",
                        staticStyle: {
                          "font-weight": "bold",
                          "text-decoration": "underline"
                        },
                        attrs: { align: "center" }
                      },
                      [_vm._v("SURAT TUGAS")]
                    ),
                    _vm._v(" "),
                    _c("h5", { ref: "nosurat", attrs: { align: "center" } }, [
                      _vm._v("No : " + _vm._s(this.no_surat))
                    ]),
                    _vm._v(" "),
                    _c(
                      "table",
                      { ref: "content", attrs: { id: "testTable" } },
                      [
                        _c("tbody", [
                          _c("tr", [
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v("Pertimbangan")
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v(":")
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { align: "justify" } }, [
                              _vm._v(
                                "\n                  Bahwa dalam rangka melaksanakan Kegiatan Pengabdian kepada Masyarakat Dosen\n                  " +
                                  _vm._s(this.prodi.nama_prodi) +
                                  " Institut Teknologi Indonesia\n                  " +
                                  _vm._s(this.semester.semester) +
                                  " ,maka dengan ini perlu dikeluarkan surat tugas\n                "
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(0),
                          _vm._v(" "),
                          _vm._m(1),
                          _vm._v(" "),
                          _vm._m(2),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v("Kepada")
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v(":")
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { align: "justify" } }, [
                              _vm._v(
                                "\n                  1. Dosen Prodi\n                  " +
                                  _vm._s(this.prodi.nama_prodi) +
                                  " (Terlampir)\n                "
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v("Untuk")
                            ]),
                            _vm._v(" "),
                            _c("td", { attrs: { valign: "top" } }, [
                              _vm._v(":")
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(
                                "\n                  1. Melaksanakan Kegiatan Pengabdian kepada Masyarakat pada\n                  " +
                                  _vm._s(this.semester.semester) +
                                  "\n                "
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(3),
                          _vm._v(" "),
                          _vm._m(4)
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "table",
                      { attrs: { id: "persetujuan", align: "right" } },
                      [
                        _c("tbody", [
                          _vm._m(5),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td"),
                            _vm._v(" "),
                            _c("td"),
                            _vm._v(" "),
                            _c(
                              "td",
                              { staticStyle: { "text-align": "right" } },
                              [
                                _c("span", [
                                  _vm._v(
                                    "Tangerang Selatan, " + _vm._s(this.tanggal)
                                  )
                                ])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._m(6),
                          _vm._v(" "),
                          _vm._m(7),
                          _vm._v(" "),
                          _vm._m(8),
                          _vm._v(" "),
                          _vm._m(9),
                          _vm._v(" "),
                          _vm._m(10),
                          _vm._v(" "),
                          _c("tr", [
                            _c("td"),
                            _vm._v(" "),
                            _c("td"),
                            _vm._v(" "),
                            _c(
                              "td",
                              { staticStyle: { "text-align": "center" } },
                              [
                                _c("p", [
                                  _vm._v(
                                    "( " + _vm._s(this.pengurus.name) + " )"
                                  )
                                ])
                              ]
                            )
                          ])
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "tembusan" }, [
                      _vm._v("\n            Tambusan Yth.\n            "),
                      _c("ol", [
                        _c("li", [
                          _vm._v(
                            "Wakil Rektor Bid. Sumberdaya, Bisnis dan Kerjasama"
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [_vm._v("Ka. Divisi SDM")]),
                        _vm._v(" "),
                        _c("li", [
                          _vm._v(
                            "\n                Prodi\n                " +
                              _vm._s(this.prodi.nama_prodi) +
                              "\n              "
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", [_vm._v("Arsip")])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "table",
                  { ref: "abdimas", attrs: { border: "1px", id: "abdimas" } },
                  [
                    _vm._m(11),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.surat, function(data, index) {
                        return _c("tr", { key: index }, [
                          _c("td", [_vm._v(_vm._s(index + 1))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.judul))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.bidang))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.name))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.dana))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.jabatan))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.keterlibatan))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.jumlah))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.SKA))]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(data.mahasiswa))])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { colspan: "3" } })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [_vm._v("Dasar")]),
      _vm._v(" "),
      _c("td", [_vm._v(":")]),
      _vm._v(" "),
      _c("td", [_vm._v("1. Kepentingan ITI")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c(
        "td",
        {
          staticStyle: {
            "text-align": "center",
            "font-weight": "bold",
            "vertical-align": "middle",
            "padding-top": "20px",
            "padding-bottom": "10px",
            "font-size": "16px"
          },
          attrs: { colspan: "3", height: "50px" }
        },
        [_c("br"), _vm._v("D I T U G A S K A N\n                  "), _c("br")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { attrs: { valign: "top" } }),
      _vm._v(" "),
      _c("td", { attrs: { valign: "top" } }),
      _vm._v(" "),
      _c("td", [_vm._v("2. Melaporkan hasil tugas kepada Direktur LP2M-ITI.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { attrs: { valign: "top" } }),
      _vm._v(" "),
      _c("td", { attrs: { valign: "top" } }),
      _vm._v(" "),
      _c("td", [_vm._v("3. Dilaksanakan dengan penuh rasa tanggung jawab.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td"),
      _vm._v(" "),
      _c("td", {
        staticStyle: { "padding-top": "10px" },
        attrs: { colspan: "2" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td"),
      _vm._v(" "),
      _c("td"),
      _vm._v(" "),
      _c("td", { staticStyle: { "text-align": "center" } }, [
        _vm._v("Lembaga Pengabdian")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td"),
      _vm._v(" "),
      _c("td"),
      _vm._v(" "),
      _c("td", { staticStyle: { "text-align": "center" } }, [
        _vm._v("dan Pemberdayaan Masyarakat")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td"),
      _vm._v(" "),
      _c("td"),
      _vm._v(" "),
      _c("td", { staticStyle: { "text-align": "center" } }, [
        _vm._v("Direktur")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { colspan: "3", height: "30px" } })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [_c("td", { attrs: { colspan: "3", height: "30px" } })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("th", [_vm._v("No")]),
      _vm._v(" "),
      _c("th", [_vm._v("TOPIK ABDIMAS")]),
      _vm._v(" "),
      _c("th", [_vm._v("BIDANG")]),
      _vm._v(" "),
      _c("th", [_vm._v("NAMA DOSEN")]),
      _vm._v(" "),
      _c("th", [_vm._v("SUMBER DANA")]),
      _vm._v(" "),
      _c("th", [_vm._v("JABATAN (KETUA/ANGGOTA)")]),
      _vm._v(" "),
      _c("th", [_vm._v("KETERLIBATAN PRODI/INSTITUSI LAIN")]),
      _vm._v(" "),
      _c("th", [_vm._v("DANA")]),
      _vm._v(" "),
      _c("th", [_vm._v("SKA")]),
      _vm._v(" "),
      _c("th", [_vm._v("KETERLIBATAN MAHASISWA")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./public/img/kop-surat.jpg":
/*!**********************************!*\
  !*** ./public/img/kop-surat.jpg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/kop-surat.jpg?32e1f453462ed5d9b1c19f0cf07b2d9f";

/***/ }),

/***/ "./resources/js/views/surat/SuratTugas.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/surat/SuratTugas.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true& */ "./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true&");
/* harmony import */ var _SuratTugas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SuratTugas.vue?vue&type=script&lang=js& */ "./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& */ "./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SuratTugas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d8b2e0c0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/surat/SuratTugas.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SuratTugas.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=style&index=0&id=d8b2e0c0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_style_index_0_id_d8b2e0c0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/surat/SuratTugas.vue?vue&type=template&id=d8b2e0c0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SuratTugas_vue_vue_type_template_id_d8b2e0c0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);